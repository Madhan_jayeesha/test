

#!/bin/bash
GIT_RESULT=$(git fetch)
echo  "GIT_RESULT-----   "$GIT_RESULT
echo
GIT_STATUS=$(git status)
echo "GIT_STATUS----     "$GIT_STATUS
echo
if [[ "$GIT_STATUS" =~  "Your branch is up to date" ]]; then

    GIT_TAG=$(git describe --tags --match "v[0-9].[0-9].[0-9]*" --abbrev=0)
    echo "GIT_TAG:    " $GIT_TAG;
    VERSION_FROM_TAG=${GIT_TAG//v}                       #remove v from the tag
    echo "VERSION_FROM_TAG:    " $VERSION_FROM_TAG

    IFS=. read -r v1 v2 v3 <<< "${VERSION_FROM_TAG}"    # split into (integer) components
 
   # dev
   if [ "$1" = "dev" ]; then
      echo
      echo "Updating server package.json version for dev"
      echo
      cd server/
      CURRENT_VERSION=$(node -p "require('./package.json').version") 
      NEW_VERSION="dev-"
      NEW_VERSION+=`date +%Y.%m.%d-%H.%M.%Z`

      echo "CURRENT_VERSION:  "$CURRENT_VERSION
      echo

      echo "NEW_VERSION:  "$NEW_VERSION
      echo
      # replacing version in package.json with NEW_VERSION
      sed -i '' 's/\"version\":.*/\"version\": "'${NEW_VERSION}'",/' "package.json"

      echo "Updating admin package.json version for dev"
      echo
      cd ../admin/
      CURRENT_VERSION=$(node -p "require('./package.json').version") 
      echo "CURRENT_VERSION:  "$CURRENT_VERSION
      echo

      echo "NEW_VERSION:  "$NEW_VERSION
      echo
      # replacing version in package.json with NEW_VERSION
      sed -i '' 's/\"version\":.*/\"version\": "'${NEW_VERSION}'",/' "package.json"


      echo "Updating client package.json version for dev"
      echo
      cd ../client/
      CURRENT_VERSION=$(node -p "require('./package.json').version") 
      echo "CURRENT_VERSION:  "$CURRENT_VERSION
      echo

      echo "NEW_VERSION:  "$NEW_VERSION
      echo
      # replacing version in package.json with NEW_VERSION
      sed -i '' 's/\"version\":.*/\"version\": "'${NEW_VERSION}'",/' "package.json"

      cd ../
      echo "....Pushing into Git...."
      echo   
      # pushing changes into git with NEW_VERSION as tag
      git add */package*
      git commit -m "$NEW_VERSION"
      while true; do
            read -p "Do you want to push your changes(yes/no)?" yn
               case $yn in
                     [Yy]* )
                        git push;
                        git tag "v$NEW_VERSION";
                        break;;

                     [Nn]* )
                        exit;;

                     * ) 
                        echo "Please answer yes or no.";;
               esac
      done



   elif [ "$1" = "major" ]; then
      echo
      echo "Updating server package.json version for major"
      echo
      ((v1++))                                                 
      NEW_VERSION="${v1}.0.0" 

      cd server/
      CURRENT_VERSION=$(node -p "require('./package.json').version") 

      echo "CURRENT_VERSION:  "$CURRENT_VERSION
      echo

      echo "NEW_VERSION:  "$NEW_VERSION
      echo
      # replacing version in package.json with NEW_VERSION
      sed -i '' 's/\"version\":.*/\"version\": "'${NEW_VERSION}'",/' "package.json"

      echo "Updating admin package.json version for major"
      echo
      cd ../admin/
      CURRENT_VERSION=$(node -p "require('./package.json').version") 

      echo "CURRENT_VERSION:  "$CURRENT_VERSION
      echo

      echo "NEW_VERSION:  "$NEW_VERSION
      echo
      # replacing version in package.json with NEW_VERSION
      sed -i '' 's/\"version\":.*/\"version\": "'${NEW_VERSION}'",/' "package.json"


      echo "Updating client package.json version for major"
      echo
      cd ../client/
      CURRENT_VERSION=$(node -p "require('./package.json').version") 
      echo "CURRENT_VERSION:  "$CURRENT_VERSION
      echo

      echo "NEW_VERSION:  "$NEW_VERSION
      echo
      # replacing version in package.json with NEW_VERSION
      sed -i '' 's/\"version\":.*/\"version\": "'${NEW_VERSION}'",/' "package.json"

      cd ../
      echo "....Pushing into Git...."
      echo   
      # pushing changes into git with NEW_VERSION as tag
      git add */package*
      git commit -m "v$NEW_VERSION"
      while true; do
            read -p "Do you want to push your changes(yes/no)?" yn
               case $yn in
                     [Yy]* )
                        git push;
                        git tag "v$NEW_VERSION";
                        break;;

                     [Nn]* )
                        exit;;

                     * ) 
                        echo "Please answer yes or no.";;
               esac
      done


    elif [ "$1" = "minor" ]; then
        echo
        echo "Updating server package.json version for minor"
        echo
        ((v2++))  
        NEW_VERSION="${v1}.${v2}.0" 

        cd server/
        CURRENT_VERSION=$(node -p "require('./package.json').version") 

        echo "CURRENT_VERSION:  "$CURRENT_VERSION
        echo

        echo "NEW_VERSION:  "$NEW_VERSION
        echo
        # replacing version in package.json with NEW_VERSION
        sed -i '' 's/\"version\":.*/\"version\": "'${NEW_VERSION}'",/' "package.json"

        echo "Updating admin package.json version for minor"
        echo
        cd ../admin/
        CURRENT_VERSION=$(node -p "require('./package.json').version") 

        echo "CURRENT_VERSION:  "$CURRENT_VERSION
        echo

        echo "NEW_VERSION:  "$NEW_VERSION
        echo
        # replacing version in package.json with NEW_VERSION
        sed -i '' 's/\"version\":.*/\"version\": "'${NEW_VERSION}'",/' "package.json"


        echo "Updating client package.json version for minor"
        echo
        cd ../client/
        CURRENT_VERSION=$(node -p "require('./package.json').version") 
        echo "CURRENT_VERSION:  "$CURRENT_VERSION
        echo

        echo "NEW_VERSION:  "$NEW_VERSION
        echo
        # replacing version in package.json with NEW_VERSION
        sed -i '' 's/\"version\":.*/\"version\": "'${NEW_VERSION}'",/' "package.json"

        cd ../
        echo "....Pushing into Git...."
        echo   
        #pushing changes into git with NEW_VERSION as tag
        git add */package*
        git commit -m "v$NEW_VERSION"
        while true; do
            read -p "Do you want to push your changes(yes/no)?" yn
               case $yn in
                     [Yy]* )
                        git push;
                        git tag "v$NEW_VERSION";
                        break;;

                     [Nn]* )
                        exit;;

                     * ) 
                        echo "Please answer yes or no.";;
               esac
         done

   elif [ "$1" = "patch" ]; then
        echo    
        echo "Updating server package.json version for patch"
        echo
        ((v3++))  
        NEW_VERSION="${v1}.${v2}.${v3}" 

        cd server/
        CURRENT_VERSION=$(node -p "require('./package.json').version") 

        echo "CURRENT_VERSION:  "$CURRENT_VERSION
        echo

        echo "NEW_VERSION:  "$NEW_VERSION
        echo
        # replacing version in package.json with NEW_VERSION
        sed -i '' 's/\"version\":.*/\"version\": "'${NEW_VERSION}'",/' "package.json"

        echo "Updating admin package.json version for patch"
        echo
        cd ../admin/
        CURRENT_VERSION=$(node -p "require('./package.json').version") 

        echo "CURRENT_VERSION:  "$CURRENT_VERSION
        echo

        echo "NEW_VERSION:  "$NEW_VERSION
        echo
        # replacing version in package.json with NEW_VERSION
        sed -i '' 's/\"version\":.*/\"version\": "'${NEW_VERSION}'",/' "package.json"


        echo "Updating client package.json version for patch"
        echo
        cd ../client/
        CURRENT_VERSION=$(node -p "require('./package.json').version") 
        echo "CURRENT_VERSION:  "$CURRENT_VERSION
        echo

        echo "NEW_VERSION:  "$NEW_VERSION
        echo
        # replacing version in package.json with NEW_VERSION
        sed -i '' 's/\"version\":.*/\"version\": "'${NEW_VERSION}'",/' "package.json"

        cd ../
        echo "....Pushing into Git...."
        echo   
        #pushing changes into git with NEW_VERSION as tag
        git add */package*
        git commit -m "v$NEW_VERSION"
        while true; do
            read -p "Do you want to push your changes(yes/no)?" yn
               case $yn in
                     [Yy]* )
                        git push;
                        git tag "v$NEW_VERSION";
                        break;;

                     [Nn]* )
                        exit;;

                     * ) 
                        echo "Please answer yes or no.";;
               esac
         done

   else
      echo "Invalid argument"
      exit 1
   
   echo
   echo "....Script Executed Successfully...."
   fi
else
   echo
   echo $GIT_RESULT
   echo "...NEW CHANGES FOUND FROM REMOTE..."
   echo "...PLEASE REVIEW THE CHANGES AND TRY AGAIN...."
fi
