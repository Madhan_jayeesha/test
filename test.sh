
GIT_BRANCH="master"
GIT_TAG=$(git describe --abbrev=0 --tags --match="*$(git tag | grep -E "^v[0-9]+\.[0-9]+\.[0-9]+$"  | sort -V -r | head -n 1)")
echo $GIT_TAG