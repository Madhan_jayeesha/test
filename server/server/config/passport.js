import passport from 'passport';
import User from '../models/user.model';
import userCtrl from '../controllers/user.controller';
// JwtStrategy = require('passport-jwt').Strategy,
// ExtractJwt = require('passport-jwt').ExtractJwt,
// LocalStrategy = require('passport-local'),
const TwitterStrategy = require('passport-twitter').Strategy;

import config from './config';
// Setting username field to email rather than username
// const localOptions = {
//   usernameField: 'email'
// };

// // Setting up local login strategy
// const localLogin = new LocalStrategy(localOptions, (email, password, done) => {
//   User.findOne({ email }, (err, user) => {
//     if (err) { return done(err); }
//     if (!user) { return done(null, false, { error: 'Your login details could not be verified. Please try again.' }); }

//     user.comparePassword(password, (err, isMatch) => {
//       if (err) { return done(err); }
//       if (!isMatch) { return done(null, false, { error: 'Your login details could not be verified. Please try again.' }); }

//       return done(null, user);
//     });
//   });
// });
// // Setting JWT strategy options
// const jwtOptions = {
//   // Telling Passport to check authorization headers for JWT
//   jwtFromRequest: ExtractJwt.fromAuthHeader(),
//   // Telling Passport where to find the secret
//   secretOrKey: config.jwtSecret
//   // TO-DO: Add issuer and audience checks
// };


// // Setting up JWT login strategy
// const jwtLogin = new JwtStrategy(jwtOptions, (payload, done) => {
//   User.findById(payload._id, (err, user) => {
//     if (err) { return done(err, false); }
//     if (user)
//       done(null, user);
//     else
//       done(null, false);
//   });
// });
passport.serializeUser(function (user, done) {
  done(null, user);
});

passport.deserializeUser(function (obj, done) {
  done(null, obj);
});

const twitterLogin = new TwitterStrategy({
  consumerKey: process.env.TWITTER_CONSUMER_KEY,
  consumerSecret: process.env.TWITTER_CONSUMER_SECRET,
  callbackURL: process.env.TWITTER_CALLBACk,
  passReqToCallback: true
},
  async function (req, token, tokenSecret, profile, done) {
    if (profile && profile._json) {
      const { name, screen_name, profile_image_url } = profile._json
      req.userData = {
        name: name,
        twitterId: screen_name,
        imageUrl: profile_image_url
      }
      let res = {};
      await userCtrl.create(req, res)
      done(null, req.user)
    }
  }
)

// passport.use(jwtLogin);
// passport.use(localLogin);
passport.use(twitterLogin);