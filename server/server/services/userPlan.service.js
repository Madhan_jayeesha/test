import UserPlan from '../models/userPlan.model';

import sessionUtil from '../utils/session.util';
import serviceUtil from '../utils/service.util';
import dateUtil from '../utils/date.util';
class UserPlanService {
  constructor() {

  }

	/**
	 * Set the userPlan variables while creating
	 */
  async setCreatePlanVariables(req, userPlan) {
    if (sessionUtil.checkTokenInfo(req, "_id")) {
      userPlan.createdBy[sessionUtil.getTokenInfo(req, "loginType")] = sessionUtil.getTokenInfo(req, "_id");
    }
    return userPlan;
  }

	/**
	 * Set the userPlan variables while updating
	 */
  async setUpdateUserVariables(req, userPlan) {
    if (sessionUtil.checkTokenInfo(req, "_id")) {
      userPlan.updatedBy[sessionUtil.getTokenInfo(req, "loginType")] = sessionUtil.getTokenInfo(req, "_id");
    }
    userPlan.updated = new Date();
    return userPlan;
  }

  async createUserPlan(req, data) {
    let userPlan = new UserPlan();
    if (data.user) {
      if (data.user._id) userPlan.userObjId = data.user._id;
      if (data.user.email) userPlan.email = data.user.email;
    }
    if (data.plan) {
      if (data.plan._id) userPlan.planObjId = data.plan._id;
      if (data.plan.name) userPlan.planName = data.plan.name;
      if (data.plan.duration) userPlan.duration = data.plan.duration;
      if (data.plan.price) userPlan.planAmount = data.plan.price;
    }
    if (req.payment && req.payment.paymentId) userPlan.paymentId = req.payment.paymentId;
    userPlan.startDate = new Date();
    var query = { userObjId: data.user._id, status: { $in: ['Active', 'Pending'] }, active: true }
    let result = await UserPlan.find(query).sort({ created: -1 });
    if (result && result.length > 0) {
      userPlan.startDate = result[0].renewalDate;
      userPlan.status = 'Pending';
    }
    if (req.query.changePlan) {
      await UserPlan.updateMany(query, { $set: { status: 'Completed' } }, { multi: true })
      userPlan.status = 'Active';
      userPlan.startDate = new Date();
    }
    userPlan.endDate = await dateUtil.getFuturDateByMonth(userPlan.startDate, userPlan.duration);
    userPlan.renewalDate = await dateUtil.getFutureDate(1, userPlan.endDate);
    req.userPlan = await UserPlan.save(userPlan);
  }

}

export default UserPlanService;