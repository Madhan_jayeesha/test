import sessionUtil from '../utils/session.util';

/**
 * set create menu variables
 * @returns {dropdown}
 */
const setCreateDropdownVariables = (req, dropdown) => {
  if (sessionUtil.checkTokenInfo(req, '_id')) {
    if (sessionUtil.getTokenInfo(req, 'loginType') === 'employee') {
      dropdown.createdBy.employee = session.getTokenInfo(req, "_id");
      dropdown.createdBy.name = session.getTokenInfo(req, "displayName");
    }
  }
  return dropdown;
}

/**
 * set update dropdown variables
 * @returns {dropdown}
 */
const setUpdateDropdownVariables = (req, dropdown) => {
  if (sessionUtil.checkTokenInfo(req, '_id')) {
    if (sessionUtil.getTokenInfo(req, 'loginType') === 'employee') {
      dropdown.updatedBy.employee = session.getTokenInfo(req, "_id");
      dropdown.updatedBy.name = session.getTokenInfo(req, "displayName");
    }
  }
  dropdown.updated = Date.now();
  return dropdown;
}
export default {
  setCreateDropdownVariables,
  setUpdateDropdownVariables
};
