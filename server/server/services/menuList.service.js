import sessionUtil from '../utils/session.util';

/**
 * set create menu variables
 * @returns {menulist}
 */
const setCreateMenuVariables = (req, menulist) => {
  if (sessionUtil.checkTokenInfo(req, '_id')) {
    if (sessionUtil.getTokenInfo(req, 'loginType') === 'employee') {
      menulist.createdBy.employee = session.getTokenInfo(req, "_id");
      menulist.createdBy.name = session.getTokenInfo(req, "displayName");
    }
  }
  return menulist;
}

/**
 * set update menulist variables
 * @returns {menulist}
 */
const setUpdateMenuVariables = (req, menulist) => {
  if (sessionUtil.checkTokenInfo(req, '_id')) {
    if (sessionUtil.getTokenInfo(req, 'loginType') === 'employee') {
      menulist.updatedBy.employee = session.getTokenInfo(req, "_id");
      menulist.updatedBy.name = session.getTokenInfo(req, "displayName");
    }
  }
  menulist.updated = Date.now();
  return menulist;
}
export default {
  setCreateMenuVariables,
  setUpdateMenuVariables
};