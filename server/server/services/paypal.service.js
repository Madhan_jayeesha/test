import Payment from '../models/payment.model';
import UserPlan from '../models/userPlan.model';
var paypal = require('paypal-rest-sdk')

/*
data={
 'mode': "",
 'client_id': "",
 'client_secret': "",
 'intent': "",
 'return_url': "",
 'cancel_url': "",
 'total': "",
 'currency':"",
  description': ""
}
*/

/*
initialize paypal configurations
*/
async function init({ mode, clientId, clientSecret }) {
  paypal.configure({
    'mode': mode,
    'client_id': clientId,
    'client_secret': clientSecret
  });
}
/**
 * Create Paypal payment
**/
async function createPayment(data) {
  let validate = await validateDetails(data)
  if (validate && validate.error) {
    return validate
  }
  await init(data);
  var paymentReq = JSON.stringify({
    'intent': data.intent,
    'redirect_urls': {
      'return_url': data.return_url,
      'cancel_url': data.cancel_url
    },
    'payer': {
      'payment_method': 'paypal'
    },
    'transactions': [{
      'amount': {
        'total': data.amount,
        'currency': data.currency
      },
      'description': data.description
    }]
  });
  let result = await new Promise((resolve, reject) => {
    paypal.payment.create(paymentReq, function (error, payment) {
      if (error) {
        reject({
          error: "OK",
          message: error
        })
      }
      if (payment && payment.links && payment.links.length > 0)
        for (let i = 0; i < payment.links.length; i++) {
          if (payment.links[i].rel === 'approval_url') {
            resolve({
              success: "OK",
              link: payment.links[i].href
            })
            /*redirect this below link if u get succes =ok */
            // res.redirect(payment.links[i].href);
          }
        }
    });
  });
  console.log(result)
  return result;
}

/**
 * verify the payment
**/
async function paymentExecution(req, data) {
  const payerId = req.query.PayerID;
  const paymentId = req.query.paymentId;
  console.log(data)
  const executePayment = {
    "payer_id": payerId,
    "transactions": [{
      "amount": {
        'total': data.amount || data.plan.price,
        'currency': req.query.currency
      }
    }]
  };
  let result = await new Promise((resolve, reject) => {
    paypal.payment.execute(paymentId, executePayment, function (error, res) {
      if (error) {
        reject({
          error: "OK",
          message: error
        })
      } else {
        resolve({
          respCode: 200,
          respMessage: 'Transaction Success.',
          details: res
        })
      }
    });
  });
  if (result) return result;
}

/**
 * cancel the payment
**/
async function cancelPayment(req) {
  return {
    error: "OK",
    message: "Transaction cancelled."
  }
}
/**
 * validating  the payment credentials
**/
async function validateDetails(data) {
  if (!data.clientId) {
    return { error: 'OK', Message: 'Client id required.' };
  } else if (!data.clientSecret) {
    return { error: 'OK', Message: 'Client secret required.' };
  } else if (!data.amount) {
    return { error: 'OK', Message: 'Amount is required.' };
  } else if (!data.currency) {
    return { error: 'OK', Message: 'Currency is required.' };
  } else {
    return { Success: 'OK' };
  }
}

export default {
  createPayment,
  cancelPayment,
  paymentExecution,
  validateDetails
}