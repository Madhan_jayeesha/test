
import sessionUtil from '../utils/session.util';
class EmployeeService {
	constructor() {

	}

	/**
	 * Set the employee variables while creating
	 */
	async setCreateEmployeeVariables(req, employee) {
		employee.displayName = " ";
		if (req.body.firstName) {
			employee.displayName += req.body.firstName + " ";
		}
		if (req.body.lastName) {
			employee.displayName += req.body.lastName;
		}
		if (sessionUtil.checkTokenInfo(req, "_id")) {
			employee.createdBy.employee = sessionUtil.getTokenInfo(req, "_id");
			employee.createdByName = sessionUtil.getTokenInfo(req, "displayName");
		}
		employee.status = "Pending";
		return employee;
	}

	/**
	 * Set the employee variables while updating
	 */
	async setUpdateEmployeeVariables(req, employee) {
		employee.displayName = " ";
		if (req.body.firstName) {
			employee.displayName += req.body.firstName + " ";
		}
		if (req.body.lastName) {
			employee.displayName += req.body.lastName;
		}
		if (sessionUtil.checkTokenInfo(req, "_id")) {
			employee.updatedBy.employee = sessionUtil.getTokenInfo(req, "_id");
		}
		employee.updated = new Date();

		if (req.body.status && req.body.status === 'Inactive') {
			employee.disabledDate = new Date();
		}
		return employee;
	}
}

export default EmployeeService;