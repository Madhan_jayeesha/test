import Plan from '../models/plan.model';

import sessionUtil from '../utils/session.util';
import serviceUtil from '../utils/service.util';

class PlanService {
  constructor() {

  }

	/**
	 * Set the plan variables while creating
	 */
  async setCreatePlanVariables(req, plan) {
    if (sessionUtil.checkTokenInfo(req, "_id")) {
      plan.createdBy.employee = sessionUtil.getTokenInfo(req, "_id");
    }
    let planCount = await Plan.find({}).count();
    let seqNum = serviceUtil.generateSequenceNumber((planCount + 1).toString(), 9);
    plan.planId = 'PLAN' + seqNum;
    return plan;
  }

	/**
	 * Set the plan variables while updating
	 */
  async setUpdateUserVariables(req, plan) {
    if (sessionUtil.checkTokenInfo(req, "_id")) {
      plan.updatedBy.employee = sessionUtil.getTokenInfo(req, "_id");
    }
    plan.updated = new Date();
    return plan;
  }
}

export default PlanService;