import Payment from '../models/payment.model';

import sessionUtil from '../utils/session.util';
import serviceUtil from '../utils/service.util';

class PaymentService {
  constructor() {

  }
  async createPaymentRecord(req, data) {
    let payment = new Payment();
    if (data.plan.price) payment.amount = data.plan.price;
    if (req.query.currency) payment.currency = req.query.currency;
    if (data.paymentResponse) payment.response = data.paymentResponse;
    if (req.query.userId) payment.userId = req.query.userId;
    payment.paymentFrom = 'paypal';
    let paymentCount = await Payment.find({}).count();
    let seqNum = serviceUtil.generateSequenceNumber((paymentCount + 1).toString(), 9);
    payment.paymentId = 'PAY' + seqNum;
    await payment.save();
  }
}

export default PaymentService;