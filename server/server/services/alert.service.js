import Alert from '../models/alerts.model';

import AlertUtil from '../utils/alert.util';
import sessionUtil from '../utils/session.util';


/**
 * insert alert
 * @returns {alert}
 */
const insertAlert = async (req, user, transaction) => {
  let alertConfig = AlertUtil.alertConfig;
  let alert = new Alert(alertConfig[req.alertKey]);
  if (req.contextId)
    alert.contextId = req.contextId;
  else if (req.entityType && req[req.entityType] && req[req.entityType]._id)
    alert.contextId = req[req.entityType]._id

  if (req.body) {
    if (req.body.pair)
      alert.pair = req.body.pair;
    if (req.body.price)
      alert.price = req.body.price;
    if (req.body.amount)
      alert.amount = req.body.amount;
    if (req.body.currency)
      alert.currency = req.body.currency;
  }

  if (req.description) alert.description = req.description;

  if (req.type && req.type === 'Trade') {
    alert.type = 'Trade';
    alert.email = req.updatedUser.emailid;
  }
  if (alertConfig && alertConfig[req.alertKey].context !== 'LessHoldAmount') {
    if (sessionUtil.getTokenInfo(req, 'loginType') === 'employee') {
      alert.createdBy.employee = sessionUtil.getTokenInfo(req, '_id');

      if (!alert.contextId) alert.contextId = sessionUtil.getTokenInfo(req, '_id');

      alert.type = 'EMPLOYEE';
      alert.email = sessionUtil.getTokenInfo(req, 'email');
    } else if (sessionUtil.getTokenInfo(req, 'loginType') === 'user') {

      if (!alert.contextId) alert.contextId = sessionUtil.getTokenInfo(req, '_id');
      alert.createdBy.user = sessionUtil.getTokenInfo(req, '_id');
      alert.type = 'USER';
      alert.email = sessionUtil.getTokenInfo(req, 'email');
    }
    await Alert.save(alert);
    return true;
  } else if ((alertConfig && alertConfig[req.alertKey].context) === 'LessHoldAmount') {

    if (!alert.contextId) alert.contextId = user._id;
    if (sessionUtil.checkTokenInfo(req, '_id')) alert.createdBy.employee = sessionUtil.getTokenInfo(req, '_id');

    alert.type = 'USER';
    alert.email = user.emailid;
    if (transaction.transferAmount)
      alert.amount = transaction.transferAmount;
    else if (transaction.holdAmount)
      alert.amount = transaction.holdAmount;

    if (transaction.currency)
      alert.currency = transaction.currency;
    if (transaction.price)
      alert.price = transaction.price;
    if (transaction.price)
      alert.pair = transaction.pair;

    await Alert.save(alert);
  } else {
    return true;
  }
}

export default {
  insertAlert
};
