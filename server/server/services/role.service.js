import sessionUtil from '../utils/session.util';
/**
 * set role variables
 * @returns {role}
 */

const setCreateRoleVaribles = (req, role) => {
  if (sessionUtil.getTokenInfo(req, '_id')) {
    if (sessionUtil.getTokenInfo(req, 'employee') === 'employee') {
      role.createdBy = sessionUtil.getTokenInfo(req, '_id');
    };
  };
  return role;
};

/**
 * set role update variables
 * @returns {user}
 */
const setUpdateRoleVaribles = (req, role) => {
  if (sessionUtil.getTokenInfo(req, '_id')) {
    if (sessionUtil.getTokenInfo(req, 'employee') === 'employee') {
      role.updatedBy = sessionUtil.getTokenInfo(req, '_id');
    };
  };
  role.updated = Date.now();
  return role;
}

export default {
  setCreateRoleVaribles,
  setUpdateRoleVaribles,
}