
import sessionUtil from '../utils/session.util';
class UserService {
	constructor() {

	}

	/**
	 * Set the user variables while creating
	 */
	async setCreateUserVariables(req, user) {
		let details = {}
		if (req.loginFrom == "email") {
			details = req.body.details.profileObj;
			user.loginFrom = req.loginFrom;
			user.email = details.email;
		}
		else if (req.loginFrom == 'twitter') {
			details = req.userData;
			user.loginFrom = req.loginFrom;
			user.twitterId = details.twitterId;
		}
		user.displayName = details.name || "User";
		user.photo = details.imageUrl;
		return user;
	}

	/**
	 * Set the user variables while updating
	 */
	async setUpdateUserVariables(req, user) {
		if (sessionUtil.checkTokenInfo(req, "_id")) {
			user.updatedBy[getTokenInfo(req, "loginType")] = sessionUtil.getTokenInfo(req, "_id");
		}
		user.updated = new Date();
		if (req.body.status && req.body.status === 'Inactive') {
			user.disabledDate = new Date();
		}
		return user;
	}
}

export default UserService;