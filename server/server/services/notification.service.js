import Notification from '../models/notification.model';

import sessionUtil from '../utils/session.util';

/**
 * set Notification variables
 * @returns {Notification}
 */
const setCreateNotificationVaribles = (req, notification) => {
  if (sessionUtil.checkTokenInfo(req)) {
    notification.createdBy[sessionUtil.getTokenInfo(req, "loginType")] = sessionUtil.getTokenInfo(req, "_id");
    notification.userId = sessionUtil.getTokenInfo(req, "_id");
    notification.userName = sessionUtil.getTokenInfo(req, "displayName");
    notification.status = "Pending";
    notification.userEmail = sessionUtil.getTokenInfo(req, "email");
  };
  return notification;
};

/**
 * set notification update variables
 * @returns {hospital}
 */
const setUpdateNotificationVaribles = (req, notification) => {
  notification.updated = Date.now();
  if (sessionUtil.checkTokenInfo(req)) {
    notification.updatedBy[sessionUtil.getTokenInfo(req, "loginType")] = sessionUtil.getTokenInfo(req, "_id");
  };
  return notification;
};

const createNotification = async (req, user, param) => {
  let notification = new Notification();
  if (user) {
    notification.userEmail = user.email;
    notification.userName = user.displayName;
    notification.userId = user._id;
    if (param && param.notification) {
      notification.title = param.notification.title;
      notification.body = param.notification.body;
      notification.status = 'Pending';
    }
    if (sessionUtil.checkTokenInfo(req))
      notification.createdBy[sessionUtil.getTokenInfo(req, "loginType")] = sessionUtil.getTokenInfo(req, "_id");
    req.notification = await Notification.save(notification)
  }
}
export default {
  setCreateNotificationVaribles,
  setUpdateNotificationVaribles,
  createNotification
};