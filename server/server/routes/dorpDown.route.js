import express from 'express';
import asyncHandler from 'express-async-handler';

import dropDownCtrl from '../controllers/dropDown.controller';

import authPolicy from '../middlewares/authenticate';

const router = express.Router(); // eslint-disable-line new-cap

router.route('/')
  /** POST /api/dropdowns - Create new event */
  .post(asyncHandler(dropDownCtrl.create))

router.route('/')
  /** get /api/dropdown -  get all dropdown */
  .get(asyncHandler(dropDownCtrl.list));


router.route('/:dropdownId').all(authPolicy.isAllowed)

  /** get /api/dropdown -  get one event using id*/
  .get(asyncHandler(dropDownCtrl.get))

  /** put /api/dropdown -  update dropdown */
  .put(asyncHandler(dropDownCtrl.update))

  /** delete /api/dropdown -  delete event */
  .delete(asyncHandler(dropDownCtrl.remove));


router.param('dropdownId', asyncHandler(dropDownCtrl.load));

export default router;
