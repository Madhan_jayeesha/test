import express from 'express';
import validate from 'express-validation';
import paramValidation from '../config/param-validation';
import notificationCtrl from '../controllers/notification.controller';
import asyncHandler from 'express-async-handler';
import authPolicy from '../middlewares/authenticate';

const router = express.Router(); // eslint-disable-line new-cap

router.route('/').all(authPolicy.isAllowed)
  /** GET /api/notifications - Get list of notifications */
  .get(asyncHandler(notificationCtrl.list))

  /** POST /api/notifications - Create new notifications */
  .post(asyncHandler(notificationCtrl.create));

router.route('/:notificationId').all(authPolicy.isAllowed)
  /** GET /api/notifications/:notificationsId - Get notifications */
  .get(asyncHandler(notificationCtrl.get))

  /** PUT /api/notifications/:notificationsId - Update notifications */
  .put(asyncHandler(notificationCtrl.update))

  /** DELETE /api/notifications/:notificationsId - Delete notifications */
  .delete(asyncHandler(notificationCtrl.remove));

/** Load notifications when API with notificationsId route parameter is hit */
router.param('notificationId', asyncHandler(notificationCtrl.load));

export default router;