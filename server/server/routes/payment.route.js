import express from 'express';
import validate from 'express-validation';
import paramValidate from '../config/param-validation';
import paymentCtrl from '../controllers/payment.controller';
import paypalService from '../services/paypal.service';
import asyncHandler from 'express-async-handler';
import authPolicy from '../middlewares/authenticate';

const router = express.Router(); // eslint-disable-line new-cap

router.route('/createPayment')
  /** POST /api/paypal - Create  payment */
  .post(validate(paramValidate.createPayment), asyncHandler(paymentCtrl.create))

router.route('/execute')
  /** get /api/paypal - Execute payment */
  .get(validate(paramValidate.executePayment), asyncHandler(paymentCtrl.execute))

router.route('/cancel')
  /** get /api/paypal -  cancel payment */
  .get(asyncHandler(paypalService.cancelPayment));

export default router;