import express from 'express';
import validate from 'express-validation';
import paramValidate from '../config/param-validation';
import userPlanCtrl from '../controllers/userPlan.controller';
import asyncHandler from 'express-async-handler';
import authPolicy from '../middlewares/authenticate';

const router = express.Router(); // eslint-disable-line new-cap

router.route('/')
  /** POST /api/userPlans - Create new userPlans */
  .post(asyncHandler(userPlanCtrl.create))

  /** get /api/userPlans -  get all userPlans */
  .get(asyncHandler(userPlanCtrl.list));

router.route('/:userPlanId')
  /** get /api/userPlans -  get one userPlans using id*/
  .get(asyncHandler(userPlanCtrl.get))

  /** put /api/userPlans -  update userPlans */
  .put(asyncHandler(userPlanCtrl.update))

  /** delete /api/userPlans -  delete userPlans */
  .delete(asyncHandler(userPlanCtrl.remove));

router.route('/changeUserPlan').all(authPolicy.isAllowed)
  .post(validate(paramValidate.changeUserPlan), asyncHandler(userPlanCtrl.changeUserPlan))

router.param('userPlanId', asyncHandler(userPlanCtrl.load));

export default router;