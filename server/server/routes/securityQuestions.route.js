import express from 'express';
import securityQuestionsCtrl from '../controllers/securityQuestions.controller';
import asyncHandler from 'express-async-handler';
import authPolicy from '../middlewares/authenticate';

const router = express.Router(); // eslint-disable-line new-cap

router.route('/userSecurityQuestions').all(authPolicy.isAllowed)
  /** GET /api/securityQuestions/userSecurityQuestions - Get userSecurityQuestions of securityQuestions */
  .post(asyncHandler(securityQuestionsCtrl.userSecurityQuestions));

router.route('/').all(authPolicy.isAllowed)
  /** GET /api/securityQuestions - Get list of securityQuestions */
  .get(asyncHandler(securityQuestionsCtrl.list))

  /** POST /api/securityQuestions- Create new securityQuestions */
  .post(asyncHandler(securityQuestionsCtrl.create));

router.route('/:securityQuestionId').all(authPolicy.isAllowed)
  /** GET /api/securityQuestions/:securityQuestionId - Get securityQuestions */
  .get(asyncHandler(securityQuestionsCtrl.get))

  /** PUT /api/securityQuestions/:securityQuestionId - Update securityQuestions */
  .put(asyncHandler(securityQuestionsCtrl.update))

  /** DELETE /api/securityQuestions/:securityQuestionId - Delete securityQuestions */
  .delete(asyncHandler(securityQuestionsCtrl.remove));

/** Load securityQuestions when API with securityQuestionId route parameter is hit */
router.param('securityQuestionId', asyncHandler(securityQuestionsCtrl.load));

export default router;