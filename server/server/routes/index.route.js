import express from 'express';

import alertsRoutes from './alerts.route'

import authRoutes from './auth.route';

import settingsRoutes from './settings.route';

import templatesRoutes from './templates.route';

import employeeRoutes from './employee.route';

import activityRoutes from './activity.route';

import uploadRoutes from './upload.route';

import userRoutes from './user.routes';

import roleRoutes from './role.route';

import securityQuestionRoutes from './securityQuestions.route';

import ticketRoutes from './ticket.route'

import menuListRoutes from './menuList.route';

import dropDownRoutes from './dorpDown.route';

import paymentRoutes from './payment.route';

import planRoutes from './plan.routes';

import userPlanRoutes from './userPlan.route';

import cronRoutes from './cron.routes';

const router = express.Router(); // eslint-disable-line new-cap

/** GET /health-check - Check service health */
router.get('/health-check', (req, res) =>
  res.send('OK')
);

// mount auth routes at /alerts
router.use('/alerts', alertsRoutes);

// mount auth routes at /auth
router.use('/auth', authRoutes);

// mount activity routes at /activities
router.use('/activities', activityRoutes);

// mount employees routes at /employees
router.use('/employees', employeeRoutes);

//Mount menuList routes at /users
router.use('/menulists', menuListRoutes);

//Mount role routes at /users
router.use('/roles', roleRoutes);

//Mount securityQuestion routes at /users
router.use('/securityQuestions', securityQuestionRoutes);

// mount settings routes at /settings
router.use('/settings', settingsRoutes);

//Mount tickets routes at /users
router.use('/tickets', ticketRoutes);

//mount templated rputes at /templates
router.use('/templates', templatesRoutes);

//Mount upload routes at /uploads
router.use('/uploads', uploadRoutes);

//Mount user routes at /users
router.use('/users', userRoutes);

//Mount user routes at /dropdowns
router.use('/dropdowns', dropDownRoutes);

//Mount payment routes at /payments
router.use('/payments', paymentRoutes);

//Mount plan routes at /plans
router.use('/plans', planRoutes);

//Mount userplan routes at /userplans
router.use('/userplans', userPlanRoutes);

//Mount cron routes at /cron
router.use('/cron', cronRoutes);

export default router;
