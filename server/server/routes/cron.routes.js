import express from 'express';
import cronCtrl from '../controllers/cron.controller';
import asyncHandler from 'express-async-handler';

const router = express.Router(); // eslint-disable-line new-cap


router.route('/userPlanChecks')
  /** get /api/announcements -  get all announcements */
  .get(asyncHandler(cronCtrl.userPlanChecks));


export default router;