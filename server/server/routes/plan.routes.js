import express from 'express';
import validate from 'express-validation';
import paramValidate from '../config/param-validation';
import planCtrl from '../controllers/plan.controller';
import asyncHandler from 'express-async-handler';
import authPolicy from '../middlewares/authenticate';

const router = express.Router(); // eslint-disable-line new-cap

router.route('/')
  /** POST /api/plans - Create new plans */
  .post(validate(paramValidate.createPlan), asyncHandler(planCtrl.create))

  /** get /api/plans -  get all plans */
  .get(asyncHandler(planCtrl.list));

router.route('/:planId')
  /** get /api/plans -  get one plans using id*/
  .get(asyncHandler(planCtrl.get))

  /** put /api/plans -  update plans */
  .put(validate(paramValidate.updatePlan), asyncHandler(planCtrl.update))

  /** delete /api/plans -  delete plans */
  .delete(asyncHandler(planCtrl.remove));

router.param('planId', asyncHandler(planCtrl.load));

export default router;