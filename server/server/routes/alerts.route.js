import express from 'express';
import alertsCtrl from '../controllers/alerts.controller';
import asyncHandler from 'express-async-handler';
import authPolicy from '../middlewares/authenticate';

const router = express.Router(); // eslint-disable-line new-cap

router.route('/').all(authPolicy.isAllowed)
  /** GET /api/alerts - Get list of alerts */
  .get(asyncHandler(alertsCtrl.list))

  /** POST /api/alerts- Create new alerts */
  .post(asyncHandler(alertsCtrl.create));

router.route('/:alertId').all(authPolicy.isAllowed)
  /** GET /api/alerts/:alertId - Get alerts */
  .get(asyncHandler(alertsCtrl.get))

  /** PUT /api/alerts/:alertId - Update alerts */
  .put(asyncHandler(alertsCtrl.update))

  /** DELETE /api/alerts/:alertId - Delete alerts */
  .delete(asyncHandler(alertsCtrl.remove));

/** Load alerts when API with alertId route parameter is hit */
router.param('alertId', asyncHandler(alertsCtrl.load));

export default router;