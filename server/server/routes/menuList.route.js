import express from 'express';
import validate from 'express-validation';
import asyncHandler from 'express-async-handler';

import paramValidation from '../config/param-validation';

import menuListCtrl from '../controllers/menuList.controller';

import authPolicy from '../middlewares/authenticate';

const router = express.Router(); // eslint-disable-line new-cap

router.route('/').all(authPolicy.isAllowed)
  /** POST /api/menulists - Create new event */
  .post(asyncHandler(menuListCtrl.create))

  /** get /api/menulists -  get all menulists */
  .get(asyncHandler(menuListCtrl.list));


router.route('/:menulistId').all(authPolicy.isAllowed)

  /** get /api/menulists -  get one event using id*/
  .get(asyncHandler(menuListCtrl.get))

  /** put /api/menulists -  update menulists */
  .put(asyncHandler(menuListCtrl.update))

  /** delete /api/menulists -  delete event */
  .delete(asyncHandler(menuListCtrl.remove));


router.param('menulistId', asyncHandler(menuListCtrl.load));

export default router;
