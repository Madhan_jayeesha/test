import config from '../config/config';


import Activity from '../models/activity.model';
import Employee from '../models/employee.model';
import User from '../models/user.model';
import Settings from '../models/settings.model';
import EmailVerify from '../models/emailVerification.model';

import activityService from '../services/activity.service';
import EmailService from '../services/email.service';
// import socketBeforeService from '../services/socket.before.service';
import tokenService from '../services/token.service';


import dateUtil from '../utils/date.util';
import i18nUtil from '../utils/i18n.util';
import respUtil from '../utils/resp.util';
import serviceUtil from '../utils/service.util';
import sessionUtil from '../utils/session.util';

const emailService = new EmailService();
const controller = "Auth";


/**
 * login response
 * @param req
 * @param res
 * @param user
 * @returns {*}
 */
async function loginResponse(req, res, user) {
  // remove exisisting token and save new token
  await tokenService.removeTokenAndSaveNewToken(req);

  // adding login activity
  await activityService.insertActivity(req);
  return res.send(respUtil.loginSuccessResponse(req));
}

/**
 * login response
 * @param req
 * @param res
 * @param user
 * @returns {*}
 */
async function sendLoginResponse(req, res) {
  req.entityType = 'user';
  // adding login activity
  await activityService.insertActivity(req);
  // send login user count to admin users by socket
  // if (req.entityType === 'user') {
  //   socketBeforeService.sendStatsForAdminDashboard({ data: { sendLiveUsers: true } });
  // };
  return res.json(respUtil.loginSuccessResponse(req));
}

async function checkDeviceInfo(req, details) {
  if (req.body.deviceInfo) {
    let count = 0;
    if (details.deviceName.length > 0) {
      for (let deviceInfo of details.deviceName) {
        if ((deviceInfo.osName != req.body.deviceInfo.osName
          || deviceInfo.browserType != req.body.deviceInfo.browserType
          || deviceInfo.ipAddress != req.body.deviceInfo.ipAddress)) {
          count++;
        }
      }
    }
    if (count == details.deviceName.length) {
      if (details.deviceName.length < 1) {
        details.deviceName.push(req.body.deviceInfo);
        details.isDifferentDevice = false
      } else {
        let randomNumber = await serviceUtil.generateRandomNumber(100000, 999999)
        details.OTP = randomNumber
        details.OTPDate = new Date();
        emailService.sendEmailviaGrid({
          templateName: config.emailTemplates.differentDeviceLoginConfirmation,
          emailParams: {
            to: req.body.email,
            displayName: details.displayName,
            osName: req.body.deviceInfo.osName,
            browserType: req.body.deviceInfo.browserType,
            searchEngine: req.body.deviceInfo.searchEngine,
            engineVersion: req.body.deviceInfo.engineVersion,
            ipAddress: req.body.deviceInfo.ipAddress,
            OTP: details.OTP
          }
        });
        // smsService.sendSMS(details, config.messages.differentDeviceLogin)
        details.isDifferentDevice = true;
      }
      if (req.body.entityType === "employee")
        await Employee.save(details);
      else if (req.body.entityType === "user")
        await User.save(details);
    }
  }
}
/**
 * Returns jwt token if valid username and password is provided
 * @param req
 * @param res
 * @param next
 * @returns {*}
 */
async function login(req, res, next) {
  logger.info('Log:Auth Controller:login: body :' + JSON.stringify(req.body), controller);
  req.i18nKey = 'loginError';
  // check email from datbase
  if (req.body.entityType === config.commonRole.employee) {
    req.details = await Employee.findUniqueEmail(req.body.email);
  } else {
    req.i18nKey = 'invalidLoginType';
    return res.json(respUtil.getErrorResponse(req));
  }
  req.entityType = `${req.body.entityType}`;
  req.activityKey = `${req.body.entityType}LoginSuccess`;
  if (!req.details) {
    req.i18nKey = 'invalidEmail';
    return res.json(respUtil.getErrorResponse(req));
  }
  if (req.details && req.details.status && req.details.status !== 'Active') {
    req.i18nKey = 'activateYourAcount';
    return res.json(respUtil.getErrorResponse(req));
  }
  req.i18nKey = `${req.body.entityType}InactiveStatusMessage`;
  // check inactive status
  if (req.details && req.details.status && req.details.status === config.commonStatus.Inactive) {
    logger.error('Error:auth Controller:loginResponse:' + i18nUtil.getI18nMessage(req.i18nKey), controller);
    return res.json(respUtil.getErrorResponse(req));
  }

  // compare authenticate password 
  if (!req.details.authenticate(req.body.password)) {
    req.i18nKey = 'invalidPassword';
    logger.error('Error:auth Controller:login:' + i18nUtil.getI18nMessage(req.i18nKey), controller);
    return res.json(respUtil.getErrorResponse(req));
  }
  checkDeviceInfo(req, req.details);

  req.details.password = undefined;
  req.details.salt = undefined;
  req[req.body.entityType] = req.details;
  req.i18nKey = 'loginSuccessMessage';
  loginResponse(req, res, req.details);
}


/**
 * This is a protected route. Will return random number only if jwt token is provided in header.
 * @param req
 * @param res
 * @returns {*}
 */
function getRandomNumber(req, res) {
  // req.user is assigned by jwt middleware if valid token is provided
  return res.json({
    user: req.user,
    num: Math.random() * 100
  });
}

//log out for Admin and User
async function logout(req, res) {
  logger.info('Log:Auth Controller:logout: query :' + JSON.stringify(req.query), controller);
  let responseJson = {};
  let activity = new Activity();
  activity.type = 'LOGOUT';
  if (sessionUtil.getTokenInfo(req, 'loginType') === 'user') {
    activity.userId = sessionUtil.getTokenInfo(req, '_id');
    req.activityKey = 'userLogoutSuccess';
  } else if (sessionUtil.getTokenInfo(req, 'loginType') === 'employee') {
    activity.employeeId = sessionUtil.getTokenInfo(req, '_id');
    req.activityKey = 'employeeLogoutSuccess';
  }
  await activityService.insertActivity(req);
  responseJson = {
    details: req.Activity
  };

  // send login user count to admin users by socket
  // if (sessionUtil.getTokenInfo(req, 'loginType') === 'user') {
  //   socketBeforeService.sendStatsForAdminDashboard({ data: { sendLiveUsers: true } });
  // }
  //return res.json(responseJson);
  req.i18nKey = 'logoutMessage';
  logger.info('Log:auth Controller:logout:' + i18nUtil.getI18nMessage("logoutMessage"), controller);
  return res.json(respUtil.logoutSuccessResponse(req));
}

/**
 * 
 *Sends the Email for the forgot password. 
 */
async function forgotPassword(req, res) {
  let settings = await Settings.findOne({ active: true });
  //check for the account type if the account type does not exists throws the error.
  if (req.body.entityType === 'employee') {
    //Email exists check
    req.details = await Employee.findUniqueEmail(req.query.email);
    req.url = templateInfo.adminUrl;
  } else {
    req.i18nKey = 'invalidLoginType';
    return res.json(respUtil.getErrorResponse(req));
  }
  if (!req.details) {
    req.i18nKey = 'emailNotExist';
    logger.error(`Error:${req.body.entityType}:forgotPassword:'${i18nUtil.getI18nMessage('emailNotExist')}`, controller);
    return res.json(respUtil.getErrorResponse(req));
  }
  if (req.details.forgotPasswordExpireTimeStamp && req.details.forgotPasswordExpireTimeStamp >= (new Date()).getTime()) {
    req.i18nKey = 'emailAlredySent';
    logger.error('Error:user.Authorization Controller:forgotPassword:' + i18nUtil.getI18nMessage('emailNotExist'), "UserAuth");
    return res.json(respUtil.getErrorResponse(req));
  }

  //Account status check
  if (req.details && req.details.status === config.commonStatus.Inactive) {
    logger.error(`Error:${req.body.entityType}:forgotPassword:'${i18nUtil.getI18nMessage('employeeInactiveStatusMessage')}`, controller);
    req.i18nKey = `${req.body.entityType}InactiveStatusMessage`;
    return res.json(respUtil.getErrorResponse(req));
  }
  req.details.forgotPasswordExpireTimeStamp = settings && settings.forgotEmailInterval ? (new Date()).getTime() + (settings.forgotEmailInterval * 60 * 1000) : (new Date()).getTime() + (5 * 60 * 1000);
  if (req.body.entityType === 'employee')
    await Employee.save(req.details);

  req.token = serviceUtil.createCryptoRandomString(9);
  req.email = req.details.email;
  req.enEmail = serviceUtil.encodeString(req.details.email);
  emailService.createEmailVerfiyRecord(req, { type: 'forgotPassword', login: "user" })

  let templateInfo = JSON.parse(JSON.stringify(config.mailSettings));
  emailService.sendEmailviaGrid({
    templateName: config.emailTemplates.forgetPassword,
    emailParams: {
      to: req.details.email,
      userName: req.details.displayName,
      link: templateInfo.serverUrl + 'users/password?token=' + req.token
    }
  });
  req.entityType = `${req.body.entityType}`;
  req.activityKey = `${req.body.entityType}ForgotPassword`;
  activityService.insertActivity(req);
  logger.info(`Log:${req.body.entityType}:forgotPassword:${i18nUtil.getI18nMessage('mailSuccess')}`, controller);
  req.i18nKey = 'resetPasswordMail';
  let response = respUtil.successResponse(req);
  if (settings && settings.forgotEmailExpireInMin) {
    response.expire = settings.forgotEmailExpireInMin;
  } else {
    response.expire = 10;
  };
  return res.json(response);
}

/** 
 * Change the recover password or activate the account by setting the password.
 */
async function changeRecoverPassword(req, res) {
  if (!req.body.enEmail) {
    req.i18nKey = 'invalidRequest';
    return res.json(respUtil.getErrorResponse(req));
  }
  let emailVerify = await EmailVerify.findOne({ active: true, token: req.body.enEmail, type: 'forgotPassword', login: req.body.entityType });
  if (!emailVerify) {
    req.i18nKey = 'invalidRequest';
    return res.json(respUtil.getErrorResponse(req));
  }
  if (!(emailVerify.expireTimeStamp > (new Date().getTime()))) {
    return res.redirect(config.serverUrl + 'html/expire.html');
  }
  if (req.body.entityType === 'employee') {
    // converted encode string to decode
    req.details = await Employee.findUniqueEmail(emailVerify.email);
  } else {
    req.i18nKey = 'invalidLoginType';
    return res.json(respUtil.getErrorResponse(req));
  }
  // email not exists
  if (!req.details) {
    req.i18nKey = 'emailNotExist';
    logger.error(`Error:${req.body.entityType}:changeRecoverPassword:${i18nUtil.getI18nMessage('emailNotExist')}`, controller);
    return res.json(respUtil.getErrorResponse(req));
  }

  let passwordDetails = req.body;

  // active account response
  if (req.query.active) {
    if (req.details.status === config.commonStatus.Active) {
      req.i18nKey = `${req.body.entityType}AlreadyActivated`;
      logger.error(`Error:${req.body.entityType}:changeRecoverPassword:${i18nUtil.getI18nMessage('employeeAlreadyActivated')}`, controller);
      return res.json(respUtil.getErrorResponse(req));
    } else if (req.details && req.details.status === config.commonStatus.Inactive) {
      req.i18nKey = `${req.body.entityType}InactiveStatusMessage`;
      logger.error(`Error:${req.body.entityType}:changeRecoverPassword:${i18nUtil.getI18nMessage('employeeInactiveStatusMessage')}`, controller);
      return res.json(respUtil.getErrorResponse(req));
    } else {
      req.details.activatedDate = dateUtil.getTodayDate();
      req.details.status = config.commonStatus.Active;
    }
  }

  if (passwordDetails.newPassword && !(passwordDetails.newPassword === passwordDetails.confirmPassword)) {
    req.i18nKey = 'passwordsNotMatched';
    logger.error(`Error:${req.body.entityType}:changeRecoverPassword:${i18nUtil.getI18nMessage('passwordsNotMatched')}`, controller);
    return res.json(respUtil.getErrorResponse(req));
  } else if (!passwordDetails.newPassword) {
    req.i18nKey = 'newPassword';
    logger.error(`Error:${req.body.entityType}:changeRecoverPassword:${i18nUtil.getI18nMessage('newPassword')}`, controller);
    return res.json(respUtil.getErrorResponse(req));
  }
  req.details.password = passwordDetails.newPassword;
  if (req.body.entityType === 'employee') {
    await Employee.save(req.details);
  }
  req.activityKey = `${req.body.entityType}ChangePassword`;
  req.entityType = `${req.body.entityType}`;
  activityService.insertActivity(req);

  req.i18nKey = req.query.active ? 'accountactivate' : 'passwordReset';
  return res.json(respUtil.successResponse(req));
}

/**
* Change Password
* @param req
* @param res
*/
async function changePassword(req, res) {
  logger.info(`Log:auth Controller:changePassword: query :${JSON.stringify(req.query)}`, controller);

  //Password change by the acount 
  if (!req.query.adminReset && sessionUtil.checkTokenInfo(req, "_id") && sessionUtil.checkTokenInfo(req, "loginType")) {
    req.body.entityType = sessionUtil.getTokenInfo(req, "loginType");
    if (req.body.entityType === "employee") {
      req.details = await Employee.get(sessionUtil.getTokenInfo(req, "_id"));
    } else {
      req.i18nKey = 'invalidLoginType';
      return res.json(respUtil.getErrorResponse(req));
    }
  } else if (req.query.adminReset) {
    //Password reset by the Admin.
    if (req.body.entityType === "employee") {
      req.details = await Employee.get(req.query._id);
    } else if (req.body.entityType === "user") {
      req.details = await User.get(req.query._id);
    } else {
      req.i18nKey = 'invalidLoginType';
      return res.json(respUtil.getErrorResponse(req));
    }
  } else {
    req.i18nKey = 'invalidLoginType';
    return res.json(respUtil.getErrorResponse(req));
  }
  let passwordDetails = req.body;
  if (!req.details) {
    req.i18nKey = "detailsNotFound";
    return res.json(respUtil.getErrorResponse(req));
  }
  let collection = req.details;
  // check new password exists
  if (passwordDetails.newPassword) {

    // check current password and new password are same
    if (passwordDetails.currentPassword && (passwordDetails.currentPassword === passwordDetails.newPassword)) {
      req.i18nKey = 'currentOldSameMsg';
      logger.error(`Error:${req.body.entityType} Controller:changePassword:' ${i18nUtil.getI18nMessage('currentOldSameMsg')}`, controller);
      return res.json(respUtil.getErrorResponse(req));
    }
    // authenticate current password
    if (collection.authenticate(passwordDetails.currentPassword)) {
      if (passwordDetails.newPassword === passwordDetails.confirmPassword) {
        collection.password = passwordDetails.newPassword;
        if (req.body.entityType === "employee") {
          collection = await Employee.save(collection);
        }
        req.activityKey = `${req.body.entityType}ChangePassword`;
        req.i18nKey = 'passwordSuccess';
        logger.info(`Log:${req.body.entityType} Controller:changePassword: ${i18nUtil.getI18nMessage('passwordSuccess')}`, controller);
        return res.json(respUtil.successResponse(req));
      } else {
        req.i18nKey = 'passwordsNotMatched';
        logger.error(`Error:${req.body.entityType} Controller:changePassword:' ${i18nUtil.getI18nMessage('passwordsNotMatched')}`, controller);
        return res.json(respUtil.getErrorResponse(req));
      }
    } else {
      req.i18nKey = 'currentPasswordError';
      logger.error(`Error:${req.body.entityType} Controller:changePassword:' ${i18nUtil.getI18nMessage('currentPasswordError')}`, controller);
      return res.json(respUtil.getErrorResponse(req));
    }
  } else {
    req.i18nKey = 'newPassword';
    logger.error(`Error:${req.body.entityType} Controller:changePassword:' ${i18nUtil.getI18nMessage('newPassword')}`, controller);
    return res.json(respUtil.getErrorResponse(req));
  }
}

async function checkForgotPasswordLink(req, res) {
  if (!(req.query && req.query.token)) {
    return res.redirect(config.serverUrl + 'html/expire.html');
  }
  let emailVerify = await EmailVerify.findOne({ active: true, token: req.query.token, type: 'forgotPassword', login: req.body.entityType });
  if (!emailVerify) {
    return res.redirect(config.serverUrl + 'html/expire.html');
  }
  if (!(emailVerify.expireTimeStamp > (new Date().getTime()))) {
    return res.redirect(config.serverUrl + 'html/expire.html');
  }
  let user = await User.findUniqueEmail(emailVerify.email);
  // email not exists
  if (!user) {
    logger.error('Error:user.Authorization Controller:changeRecoverPassword:' + i18nUtil.getI18nMessage('emailNotExist'), "UserAuth");
    return res.redirect(config.serverUrl + 'html/expire.html');
  }
  let templateInfo = JSON.parse(JSON.stringify(config.mailSettings));
  res.redirect(templateInfo.clientUrl + 'changerecoverpassword/' + req.query.token);
};

const successRedirect = async (req, res) => {
  if (req.user) {
    req.entityType = 'user';
    req.i18nKey = 'loginSuccessMessage';
    loginResponse(req, res, req.user);
  }
}

export default {
  login,
  getRandomNumber,
  logout,
  loginResponse,
  sendLoginResponse,
  forgotPassword,
  changeRecoverPassword,
  changePassword,
  checkForgotPasswordLink,
  successRedirect
};
