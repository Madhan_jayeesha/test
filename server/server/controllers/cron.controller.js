import nodeCron from 'node-cron';

import UserPlan from '../models/userPlan.model';

nodeCron.schedule("0 0 * * *", function () {
  let req = {}, res = {};
  userPlanChecks(req, res)
});

const userPlanChecks = async (req, res) => {
  let criteria = { renewalDate: { $lte: Date.now() }, active: true, status: 'Active' };
  await UserPlan.updateMany(criteria, { $set: { status: 'Completed' } }, { multi: true });
  criteria = { startDate: { $lte: Date.now() }, active: true, status: 'Pending' };
  await UserPlan.updateMany(criteria, { $set: { status: 'Active' } }, { multi: true });
  res.send({ respMessage: 'SUCCESS' })
}

export default {
  userPlanChecks
}

