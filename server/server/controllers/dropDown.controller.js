import DropDown from '../models/dropDown.model';

import dropDownService from '../services/dropDown.service';

import i18nUtil from '../utils/i18n.util';
import respUtil from '../utils/resp.util';
import serviceUtil from '../utils/service.util';

const controller = "DropDown";

/**
 * Load dropDown and append to req.
 * @param req
 * @param res
 * @param next
 */
async function load(req, res, next) {
  req.dropDown = await DropDown.get(req.params.dropDownId);
  return next();
}

/**
 * Get dropDown
 * @param req
 * @param res
 * @returns {DropDown}
 */
async function get(req, res) {
  logger.info('Log:dropDown Controller:get: query :' + JSON.stringify(req.query), controller);

  await serviceUtil.checkPermission(req, res, "View", controller);
  let responseJson = {};
  logger.info('Log:dropDown Controller:' + i18nUtil.getI18nMessage('recordFound'), controller);
  responseJson.respCode = respUtil.getDetailsSuccessResponse().respCode;
  responseJson.details = req.dropDown;
  return res.json(responseJson);
}

/**
 * Create new dropDown
 * @param req
 * @param res
 * @returns { respCode: respCode, respMessage: respMessage }
 */
async function create(req, res) {
  logger.info('Log:Auth Controller:create: body :' + JSON.stringify(req.body), controller);
  await serviceUtil.checkPermission(req, res, "Edit", controller);
  let dropDown = new DropDown(req.body);
  dropDown = await dropDownService.setCreateDropdownVariables(req, dropDown);
  req.dropDown = await DropDown.save(dropDown);
  req.entityType = 'dropDown';
  logger.info('Log:dropDown Controller:' + i18nUtil.getI18nMessage('dropDownCreate'), controller);
  return res.json(respUtil.createSuccessResponse(req));
}

/**
 * Update existing dropDown
 * @param req
 * @param res
 * @param next
 * @returns { respCode: respCode, respMessage: respMessage }
 */
async function update(req, res, next) {
  logger.info('Log:dropDown Controller:update: body :' + JSON.stringify(req.body), controller);
  await serviceUtil.checkPermission(req, res, "Edit", controller);
  let dropDown = req.dropDown;
  dropDown = Object.assign(dropDown, req.body);
  dropDown = await dropDownService.setUpdateDropdownVariables(req, dropDown);
  req.dropDown = await DropDown.save(dropDown);
  req.entityType = 'dropDown';
  logger.info('Log:dropDown Controller:' + i18nUtil.getI18nMessage('dropDownUpdate'), controller);
  return res.json(respUtil.updateSuccessResponse(req));
}

/**
 * Get dropDown list. based on criteria
 * @param req
 * @param res
 * @param next
 * @returns {dropDowns: dropDowns, pagination: pagination}
 */
async function list(req, res, next) {
  logger.info('Log:dropDown Controller:list: query :' + JSON.stringify(req.query), controller);
  await serviceUtil.checkPermission(req, res, "View", controller);
  let responseJson = {};
  const query = await serviceUtil.generateListQuery(req);
  //get total dropDowns
  if (query.page === 1)
    // total count 
    query.pagination.totalCount = await DropDown.totalCount(query);

  if (req && req.query && req.query.type)
    query.filter.type = req.query.type;

  const dropDowns = await DropDown.list(query);
  logger.info('Log:dropDown Controller:' + i18nUtil.getI18nMessage('recordsFound'), controller);
  responseJson.respCode = respUtil.getDetailsSuccessResponse().respCode;
  responseJson.dropDowns = dropDowns;
  responseJson.pagination = query.pagination;
  return res.json(responseJson)
}

/**
 * Delete dropDown.
 * @param req
 * @param res
 * @param next
 * @returns { respCode: respCode, respMessage: respMessage }
 */
async function remove(req, res, next) {
  logger.info('Log:dropDown Controller:remove: query :' + JSON.stringify(req.query), controller);
  await serviceUtil.checkPermission(req, res, "Edit", controller);
  const dropDown = req.dropDown;
  dropDown.active = false;
  req.dropDown = await DropDown.save(dropDown);
  req.entityType = 'dropDown';
  logger.info('Log:dropDown Controller:' + i18nUtil.getI18nMessage('dropDownDelete'), controller);
  return res.json(respUtil.removeSuccessResponse(req));
}

export default {
  load,
  get,
  create,
  update,
  list,
  remove
};
