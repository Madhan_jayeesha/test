import MenuList from '../models/menuList.model';

import activityService from '../services/activity.service';
import menulistService from '../services/menuList.service';
import respUtil from '../utils/resp.util';
import serviceUtil from '../utils/service.util';
import Role from '../models/role.model';

const controller = "MenuList";

/**
 * Load menulist and append to req.
 * @param req
 * @param res
 * @param next
 */

async function load(req, res, next) {
  req.menulist = await MenuList.get(req.params.menulistId);
  return next();
}

/**
 * Get menulist record by id
 * @param req
 * @param res
 * @returns {details: menulist}
 */
async function get(req, res) {
  let menulist = req.menulist;
  let responseJson = {};
  logger.info('Log:menulist Controller:get: query :' + JSON.stringify(req.query), controller);

  await serviceUtil.checkPermission(req, res, "View", controller);
  responseJson.details = menulist;
  return res.json(responseJson);
};
/**
 * Create new menulist
 * @param req
 * @param res
 * @returns { respCode: respCode, respMessage: respMessage }
 */

async function create(req, res) {
  logger.info('Log:menulist Controller:create: body :' + JSON.stringify(req.query), controller);

  await serviceUtil.checkPermission(req, res, "Edit", controller);
  let menulist = new MenuList(req.body);
  menulist = await menulistService.setCreateMenuVariables(req, menulist);
  let roles = await Role.find({ active: true });
  if (roles) {
    for (let role of roles) {
      let obj = JSON.parse(JSON.stringify(role.permissions));
      obj[`${menulist.label}`] = "noView";
      role.permissions = obj;
      await Role.save(role);
    }
  }
  req.menulist = await MenuList.save(menulist);
  req.entityType = 'menulist';
  req.activityKey = 'menuListCreate';
  //adding new menulist  create Activity
  activityService.insertActivity(req);
  return res.json(respUtil.createSuccessResponse(req));
}

/**
 * get menulist list.based on criteria
 * @param req
 * @param res
 * @param next
 * @returns {menulist: menulist, pagination: pagination}
 */

async function list(req, res) {
  let responseJson = {};
  logger.info('Log:menulist Controller:list: query :' + JSON.stringify(req.query), controller);

  await serviceUtil.checkPermission(req, res, "View", controller);
  req.entityType = 'menulist';
  const query = await serviceUtil.generateListQuery(req);
  //get total  records
  if (query.page === 1)
    // total count 
    query.pagination.totalCount = await MenuList.totalCount(query);

  const menulist = await MenuList.list(query);
  responseJson.respCode = respUtil.getDetailsSuccessResponse().respCode;
  responseJson.menus = menulist;
  responseJson.pagination = query.pagination;
  return res.json(responseJson);
}

/**
 * update existing menulist
 * @param req
 * @param res
 * @param next
 * @returns { respCode: respCode, respMessage: respMessage }
 * */

async function update(req, res, next) {
  logger.info('Log:menulist Controller:update: body :' + JSON.stringify(req.query), controller);

  await serviceUtil.checkPermission(req, res, "Edit", controller);
  let menulist = req.menulist;
  menulist = await menulistService.setUpdateMenuVariables(req, menulist);
  menulist = Object.assign(menulist, req.body);
  req.menulist = await MenuList.save(menulist);
  req.entityType = 'menulist';
  req.activityKey = 'menuListUpdate';
  // adding menulist update activity
  activityService.insertActivity(req);
  return res.json(respUtil.updateSuccessResponse(req));
}

/**
  * Delete menulist.
 * @param req
 * @param res
 * @param next
 * @returns { respCode: respCode, respMessage: respMessage }
 */

async function remove(req, res, next) {
  logger.info('Log:menulist Controller:remove: query :' + JSON.stringify(req.query), controller);
  await serviceUtil.checkPermission(req, res, "Edit", controller);
  let menulist = req.menulist;
  menulist.updated = new Date();
  menulist.active = false;
  req.menulist = await MenuList.save(menulist);
  req.entityType = 'menulist';
  req.activityKey = 'menuListDelete';
  // adding menulist delete activity
  activityService.insertActivity(req);
  return res.json(respUtil.removeSuccessResponse(req));
}

export default {
  load,
  create,
  get,
  update,
  list,
  remove
};
