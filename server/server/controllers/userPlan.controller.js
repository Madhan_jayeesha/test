import UserPlan from '../models/userPlan.model';

import activityService from '../services/activity.service';
import UserPlanService from '../services/userPlan.service';

import i18nUtil from '../utils/i18n.util';
import respUtil from '../utils/resp.util';
import serviceUtil from '../utils/service.util';
import sessionUtil from '../utils/session.util';

const userPlanService = new UserPlanService();

const controller = "UserPlan";

/**
 * Load Plan and append to req.
 * @param req
 * @param res
 * @param next
 */

async function load(req, res, next) {
  req.userPlan = await UserPlan.get(req.params.userPlanId);
  return next();
}

/**
 * Get Plan
 * @param req
 * @param res
 * @returns {details: userPlan}
 */

async function get(req, res) {
  logger.info('Log:userPlan Controller:get: query :' + JSON.stringify(req.query), controller);

  await serviceUtil.checkPermission(req, res, "View", controller);
  req.query = await serviceUtil.generateListQuery(req);
  let userPlan = req.userPlan;
  logger.info('Log:userPlan Controller:get:' + i18nUtil.getI18nMessage('recordFound'), controller);
  let responseJson = {
    respCode: respUtil.getDetailsSuccessResponse().respCode,
    details: userPlan
  };
  return res.json(responseJson);
}

/**
 * Create new userPlan
 * @param req
 * @param res
 * @returns { respCode: respCode, respMessage: respMessage }
 */
async function create(req, res) {
  logger.info('Log:userPlan Controller:create: body :' + JSON.stringify(req.body), controller);

  // await serviceUtil.checkPermission(req, res, "Edit", controller);
  if (req.body) {
    let userPlan = new UserPlan(req.body);
    userPlan = await userPlanService.setCreatePlanVariables(req, userPlan);
    req.userPlan = await UserPlan.save(userPlan);
    req.entityType = 'userPlan';
    req.activityKey = 'userPlanCreate';
    activityService.insertActivity(req);
    logger.info('Log:userPlan Controller:create:' + i18nUtil.getI18nMessage('userPlanCreate'), controller);
    return res.json(respUtil.createSuccessResponse(req));
  }
}

/**
* Get userPlan list. based on criteria
* @param req
* @param res
* @param next
* @returns {userPlans: userPlans, pagination: pagination}
*/
async function list(req, res, next) {
  let responseJson = {};
  logger.info('log:userPlan Controller:list:query :' + JSON.stringify(req.query), controller);
  // await serviceUtil.checkPermission(req, res, "View", controller);
  const query = await serviceUtil.generateListQuery(req);

  if (query.page === 1)
    // total count
    query.pagination.totalCount = await Plan.totalCount(query);

  //get userPlan records
  const userPlans = await UserPlan.list(query);
  logger.info('Log:userPlan Controller:list:' + i18nUtil.getI18nMessage('recordsFound'), controller);
  responseJson.respCode = respUtil.getDetailsSuccessResponse().respCode;
  responseJson.userPlans = userPlans;
  responseJson.pagination = query.pagination;
  return res.json(responseJson);
}


/**
 * Update existing userPlan
 * @param req
 * @param res
 * @param next
 * @returns { respCode: respCode, respMessage: respMessage }
 */
async function update(req, res, next) {
  req.query = await serviceUtil.generateListQuery(req);
  logger.info('Log:userPlan Controller:update: body :' + JSON.stringify(req.body), controller);

  // await serviceUtil.checkPermission(req, res, "Edit", controller);
  let userPlan = req.userPlan;
  userPlan = Object.assign(userPlan, req.body);
  userPlan = await userPlanService.setUpdatePlanVariables(req, userPlan)
  req.userPlan = await UserPlan.save(userPlan);
  req.entityType = 'userPlan';
  req.activityKey = 'userPlanUpdate';
  activityService.insertActivity(req);
  logger.info('Log:userPlan Controller:update:' + i18nUtil.getI18nMessage('userPlanUpdate'), controller);
  return res.json(respUtil.updateSuccessResponse(req));
}

/**
 * Delete userPlan.
 * @param req
 * @param res
 * @param next
 * @returns { respCode: respCode, respMessage: respMessage }
 */
async function remove(req, res, next) {
  logger.info('Log:userPlan Controller:remove: query :' + JSON.stringify(req.query), controller);

  // await serviceUtil.checkPermission(req, res, "Edit", controller);
  let userPlan = req.userPlan;
  userPlan = await userPlanService.setUpdatePlanVariables(req, userPlan)
  userPlan.active = false;
  req.userPlan = await UserPlan.save(userPlan);
  req.entityType = 'userPlan';
  req.activityKey = 'userPlanDelete';
  activityService.insertActivity(req);
  logger.info('Log:userPlan Controller:remove:' + i18nUtil.getI18nMessage('userPlanDelete'), controller);
  res.json(respUtil.removeSuccessResponse(req));
}

const changeUserPlan = async (req, res) => {
  logger.info('Log:userPlan Controller:changePlan: query :' + JSON.stringify(req.query), controller);
  let plan = await Plan.findOne(req.body.planId);
  if (!plan) {
    req.i18nKey = 'InvalidPlanId';
    return res.json(respUtil.getErrorResponse(req))
  }
  var query = [
    { $match: { email: data.user.email, status: 'Pending' } },
    {
      $group: {
        '_id': "$userObjId",
        total: { $sum: "$planAmount" }
      }
    }
  ]
  let result = await UserPlan.aggregate(query);
  if (plan.price > result.total) {
    plan.price -= result.total;
    return res.json({
      success: "OK",
      amount: plan.price
    })
  }
  else if (plan.price <= result.total) {
    let data = {};
    data.plan = plan;
    let user = await UserPlan.findOne({ _id: sessionUtil.getTokenInfo(req, '_id') });
    if (user)
      data.user = user;
    await userPlanService.createUserPlan(req, data);
    req.entityType = 'userPlan';
    return res.json(respUtil.createSuccessResponse(req))
  }
}

export default {
  create,
  list,
  get,
  load,
  update,
  remove,
  changeUserPlan
};
