import Alert from '../models/alerts.model';

import activityService from '../services/activity.service';
import errorService from '../services/error.service';

import i18nUtil from '../utils/i18n.util';
import respUtil from '../utils/resp.util';
import serviceUtil from '../utils/service.util';

const controller = "Alerts";

/**
 * Load alert and append to req.
 * @param req
 * @param res
 * @param next
 */
async function load(req, res, next) {
  req.alert = await Alert.get(req.params.alertId);
  return next();
}

/**
 * Get alert
 * @param req
 * @param res
 * @returns {details: alert}
 */

async function get(req, res) {
  logger.info('Log:alert Controller:get: query :' + JSON.stringify(req.query), controller);

  await serviceUtil.checkPermission(req, res, "View", controller);
  req.query = await serviceUtil.generateListQuery(req);
  let alert = req.alert;
  logger.info('Log:alert Controller:get:' + i18nUtil.getI18nMessage('recordFound'), controller);
  let responseJson = {
    respCode: respUtil.getDetailsSuccessResponse().respCode,
    details: alert
  };
  return res.json(responseJson);
}
/**
 * Create new alert
 * @param req
 * @param res
 * @returns { respCode: respCode, respMessage: respMessage }
 */
async function create(req, res) {
  logger.info('Log:alert Controller:create: body :' + JSON.stringify(req.body), controller);

  await serviceUtil.checkPermission(req, res, "Edit", controller);
  let alert = new Alert(req.body);
  req.alert = await Alert.save(alert);
  req.entityType = 'alert';
  req.activityKey = 'alertCreate';
  activityService.insertActivity(req);
  req.errorKey = 'alertCreate';
  errorService.insertActivity(req);
  logger.info('Log:alert Controller:create:' + i18nUtil.getI18nMessage('alertCreate'), controller);
  return res.json(respUtil.createSuccessResponse(req));
}

/**
 * Update existing alert
 * @param req
 * @param res
 * @param next
 * @returns { respCode: respCode, respMessage: respMessage }
 */
async function update(req, res, next) {
  logger.info('Log:alert Controller:update: body :' + JSON.stringify(req.body), controller);

  await serviceUtil.checkPermission(req, res, "Edit", controller);
  let alert = req.alert;
  alert = Object.assign(alert, req.body);
  req.alert = await Alert.save(alert);
  req.entityType = 'alert';
  req.activityKey = 'alertUpdate';
  activityService.insertActivity(req);
  req.errorKey = 'alertUpdate';
  errorService.insertActivity(req);
  logger.info('Log:alert Controller:update:' + i18nUtil.getI18nMessage('alertUpdate'), controller);
  return res.json(respUtil.updateSuccessResponse(req));
}

/**
 * Get alert list. based on criteria
 * @param req
 * @param res
 * @param next
 * @returns {alert: alert, pagination: pagination}
 */
async function list(req, res, next) {
  logger.info('Log:alert Controller:list: query :' + JSON.stringify(req.query), controller);

  await serviceUtil.checkPermission(req, res, "View", controller);
  let responseJson = {};
  const query = await serviceUtil.generateListQuery(req);
  //get total alert
  if (query.page === 1)
    //  total count;
    query.pagination.totalCount = await Alert.totalCount(query);

  req.alerts = await Alert.list(query);
  responseJson.pagination = query.pagination;
  logger.info('Log:alert Controller:list:' + i18nUtil.getI18nMessage('recordsFound'), controller);
  responseJson.respCode = respUtil.getDetailsSuccessResponse().respCode;
  responseJson.alerts = req.alerts;
  return res.json(responseJson)
}

/**
 * Delete alert.
 * @param req
 * @param res
 * @param next
 * @returns { respCode: respCode, respMessage: respMessage }
 */
async function remove(req, res, next) {
  logger.info('Log:alert Controller:remove: query :' + JSON.stringify(req.query), controller);

  await serviceUtil.checkPermission(req, res, "Edit", controller);
  const alert = req.alert;
  alert.active = false;
  req.alert = await Alert.save(alert);
  req.entityType = 'alert';
  req.activityKey = 'alertDelete';
  activityService.insertActivity(req);
  logger.info('Log:alert Controller:remove:' + i18nUtil.getI18nMessage('alertDelete'), controller);
  return res.json(respUtil.removeSuccessResponse(req));
}

export default {
  load,
  get,
  create,
  update,
  list,
  remove
};