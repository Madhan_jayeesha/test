import Notification from '../models/notification.model';

import notificationService from '../services/notification.service';
import activityService from '../services/activity.service';

import respUtil from '../utils/resp.util';
import serviceUtil from '../utils/service.util';

const controller = "Notification";

/**
 * Load notification and append to req.
 * @param req
 * @param res
 * @param next
 */
async function load(req, res, next) {
  req.notification = await Notification.get(req.params.notificationId);
  return next();
}

/**
 * Get notification
 * @param req
 * @param res
 * @returns {details: Notification}
 */
async function get(req, res) {
  logger.info('Log:Notification Controller:get: query :' + JSON.stringify(req.query), controller);

  await serviceUtil.checkPermission(req, res, "View", controller);
  return res.json({
    details: req.notification
  });
}

/**
 * Create new notification
 * @param req
 * @param res
 * @returns { respCode: respCode, respMessage: respMessage }
 */
async function create(req, res) {
  logger.info('Log:Notification Controller:create: body :' + JSON.stringify(req.body), controller);

  await serviceUtil.checkPermission(req, res, "Edit", controller);
  let notification = new Notification(req.body);
  notification = notificationService.setCreateNotificationVaribles(req, notification);
  req.notification = await Notification.save(notification);
  req.entityType = 'notification';
  req.activityKey = 'notificationCreate';

  // adding notification create activity
  activityService.insertActivity(req);
  res.json(respUtil.createSuccessResponse(req));
}

/**
 * Update existing notification
 * @param req
 * @param res
 * @param next
 * @returns { respCode: respCode, respMessage: respMessage }
 */
async function update(req, res, next) {
  logger.info('Log:Notification Controller:update: body :' + JSON.stringify(req.body), controller);

  await serviceUtil.checkPermission(req, res, "Edit", controller);
  let notification = req.notification;
  notification = Object.assign(notification, req.body);
  notification = notificationService.setUpdateNotificationVaribles(req, notification);
  req.notification = await Notification.save(notification);
  req.entityType = 'notification';
  req.activityKey = 'notificationUpdate';

  // adding notification update activity
  activityService.insertActivity(req);
  res.json(respUtil.updateSuccessResponse(req));
}

/**
 * Get notification list. based on criteria
 * @param req
 * @param res
 * @param next
 * @returns {notifications: notifications, pagination: pagination}
 */
async function list(req, res, next) {
  logger.info('Log:Notification Controller:list: query :' + JSON.stringify(req.query), controller);

  await serviceUtil.checkPermission(req, res, "View", controller);
  const query = await serviceUtil.generateListQuery(req);
  if (query.page === 1)
    // total count 
    query.pagination.totalCount = await Notification.totalCount(query);

  req.entityType = 'notification';

  //get total notifications
  const notifications = await Notification.list(query);
  res.json({
    notifications: notifications,
    pagination: query.pagination
  });
}

/**
 * Delete notification.
 * @param req
 * @param res
 * @param next
 * @returns { respCode: respCode, respMessage: respMessage }
 */
async function remove(req, res, next) {
  logger.info('Log:Notification Controller:remove: query :' + JSON.stringify(req.query), controller);

  await serviceUtil.checkPermission(req, res, "Edit", controller);
  let notification = req.notification;
  notification.active = false;
  notification = notificationService.setUpdateNotificationVaribles(req, notification);
  req.notification = await Notification.save(notification);
  req.entityType = 'notification';
  req.activityKey = 'notificationDelete';

  // adding notification delete activity
  activityService.insertActivity(req);
  res.json(respUtil.removeSuccessResponse(req));
};

export default {
  load,
  get,
  create,
  update,
  list,
  remove
};