import config from '../config/config';

import User from '../models/user.model';

import authCtrl from '../controllers/auth.controller';

import activityService from '../services/activity.service';
import EmailService from '../services/email.service';
import UserService from '../services/user.service';

import i18nUtil from '../utils/i18n.util';
import respUtil from '../utils/resp.util';
import serviceUtil from '../utils/service.util';
import sessionUtil from '../utils/session.util';

const userService = new UserService();
const emailService = new EmailService();

const controller = "User";

/**
 * Load User and append to req.
 * @param req
 * @param res
 * @param next
 */

async function load(req, res, next) {
	req.user = await User.get(req.params.userId);
	return next();
}

/**
 * Get User
 * @param req
 * @param res
 * @returns {details: user}
 */

async function get(req, res) {
	logger.info('Log:User Controller:get: query :' + JSON.stringify(req.query), controller);

	await serviceUtil.checkPermission(req, res, "View", controller);
	req.query = await serviceUtil.generateListQuery(req);
	let user = req.user;
	logger.info('Log:user Controller:get:' + i18nUtil.getI18nMessage('recordFound'), controller);
	let responseJson = {
		respCode: respUtil.getDetailsSuccessResponse().respCode,
		details: user
	};
	return res.json(responseJson);
}

/**
 * Create new user
 * @param req
 * @param res
 * @returns { respCode: respCode, respMessage: respMessage }
 */
async function create(req, res) {
	logger.info('Log:User Controller:create: body :' + JSON.stringify(req.body), controller);

	await serviceUtil.checkPermission(req, res, "Edit", controller);
	let userExists;
	if (req.body && req.body.details) {
		if (req.body.details.googleId && req.body.details.profileObj && req.body.details.profileObj.email) {
			req.loginFrom = "email";
			//check email exists or not
			userExists = await User.findUniqueEmail(req.body.details.profileObj.email);
		}
	}
	else if (req.userData) {
		if (req.userData.name && req.userData.twitterId) {
			req.loginFrom = 'twitter';
			//check user exists or not
			userExists = await User.findOne({ twitterId: req.userData.twitterId });
		}
	}
	//Create new record with user ref
	req.entityType = 'user';
	if (!userExists) {
		let user = new User();
		user = await userService.setCreateUserVariables(req, user);
		req.user = await User.save(user);
		req.activityKey = 'userCreate';
		activityService.insertActivity(req);
		// emailService.createEmailVerfiyRecord(req, { type: 'userActive', login: "user" })

		//send email to user
		// emailService.sendEmail(req, res);
		// let templateInfo = JSON.parse(JSON.stringify(config.mailSettings));
		// emailService.sendEmailviaGrid({
		// 	templateName: config.emailTemplates.welcomeUser,
		// 	emailParams: {
		// 		to: user.email,
		// 		displayName: user.displayName,
		// 		Id: req.user._id,
		// 		link: templateInfo.adminUrl
		// 	}
		// });
		logger.info('Log:user Controller:create:' + i18nUtil.getI18nMessage('userCreate'), controller);
	}
	req.user = userExists;
	if (req.loginFrom == "email") {
		req.i18nKey = 'loginSuccessMessage';
		authCtrl.loginResponse(req, res, req.user);
	}
}

/**
* Get user list. based on criteria
* @param req
* @param res
* @param next
* @returns {users: users, pagination: pagination}
*/
async function list(req, res, next) {
	let responseJson = {};
	logger.info('log:User Controller:list:query :' + JSON.stringify(req.query), controller);
	await serviceUtil.checkPermission(req, res, "View", controller);
	const query = await serviceUtil.generateListQuery(req);

	if (query.page === 1)
		// total count
		query.pagination.totalCount = await User.totalCount(query);

	//get user records
	const users = await User.list(query);
	logger.info('Log:user Controller:list:' + i18nUtil.getI18nMessage('recordsFound'), controller);
	responseJson.respCode = respUtil.getDetailsSuccessResponse().respCode;
	responseJson.users = users;
	responseJson.pagination = query.pagination;
	return res.json(responseJson);
}


/**
 * Update existing user
 * @param req
 * @param res
 * @param next
 * @returns { respCode: respCode, respMessage: respMessage }
 */
async function update(req, res, next) {
	req.query = await serviceUtil.generateListQuery(req);
	logger.info('Log:User Controller:update: body :' + JSON.stringify(req.body), controller);

	await serviceUtil.checkPermission(req, res, "Edit", controller);
	let user = req.user;
	// check unique email
	if (req.body.email && user.email !== req.body.email) {
		req.i18nKey = 'emailCannotChange';
		logger.error('Error:user Controller:update:' + i18nUtil.getI18nMessage('emailExists'), controller);
		return res.json(respUtil.getErrorResponse(req));
	}
	user = Object.assign(user, req.body);
	user = await userService.setUpdateUserVariables(req, user)
	req.user = await User.save(user);
	req.entityType = 'user';
	req.activityKey = 'userUpdate';
	activityService.insertActivity(req);
	logger.info('Log:user Controller:update:' + i18nUtil.getI18nMessage('userUpdate'), controller);
	return res.json(respUtil.updateSuccessResponse(req));
}

/**
 * Delete user.
 * @param req
 * @param res
 * @param next
 * @returns { respCode: respCode, respMessage: respMessage }
 */
async function remove(req, res, next) {
	logger.info('Log:User Controller:remove: query :' + JSON.stringify(req.query), controller);

	await serviceUtil.checkPermission(req, res, "Edit", controller);
	let user = req.user;
	user = await userService.setUpdateUserVariables(req, user)
	user.active = false;
	req.user = await User.save(user);
	req.entityType = 'user';
	req.activityKey = 'userDelete';
	activityService.insertActivity(req);
	logger.info('Log:user Controller:remove:' + i18nUtil.getI18nMessage('userDelete'), controller);
	res.json(respUtil.removeSuccessResponse(req));
}

export default {
	create,
	list,
	get,
	load,
	update,
	remove
};
