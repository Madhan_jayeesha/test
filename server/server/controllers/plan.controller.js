
import Plan from '../models/plan.model';

import activityService from '../services/activity.service';
import PlanService from '../services/plan.service';

import i18nUtil from '../utils/i18n.util';
import respUtil from '../utils/resp.util';
import serviceUtil from '../utils/service.util';

const planService = new PlanService();

const controller = "Plan";

/**
 * Load Plan and append to req.
 * @param req
 * @param res
 * @param next
 */

async function load(req, res, next) {
  req.plan = await Plan.get(req.params.planId);
  return next();
}

/**
 * Get Plan
 * @param req
 * @param res
 * @returns {details: plan}
 */

async function get(req, res) {
  logger.info('Log:Plan Controller:get: query :' + JSON.stringify(req.query), controller);

  await serviceUtil.checkPermission(req, res, "View", controller);
  req.query = await serviceUtil.generateListQuery(req);
  let plan = req.plan;
  logger.info('Log:plan Controller:get:' + i18nUtil.getI18nMessage('recordFound'), controller);
  let responseJson = {
    respCode: respUtil.getDetailsSuccessResponse().respCode,
    details: plan
  };
  return res.json(responseJson);
}

/**
 * Create new plan
 * @param req
 * @param res
 * @returns { respCode: respCode, respMessage: respMessage }
 */
async function create(req, res) {
  logger.info('Log:Plan Controller:create: body :' + JSON.stringify(req.body), controller);

  // await serviceUtil.checkPermission(req, res, "Edit", controller);
  if (req.body) {
    let plan = new Plan(req.body);
    plan = await planService.setCreatePlanVariables(req, plan);
    req.plan = await Plan.save(plan);
    req.entityType = 'plan';
    req.activityKey = 'planCreate';
    activityService.insertActivity(req);
    logger.info('Log:plan Controller:create:' + i18nUtil.getI18nMessage('planCreate'), controller);
    return res.json(respUtil.createSuccessResponse(req));
  }
}

/**
* Get plan list. based on criteria
* @param req
* @param res
* @param next
* @returns {plans: plans, pagination: pagination}
*/
async function list(req, res, next) {
  let responseJson = {};
  logger.info('log:Plan Controller:list:query :' + JSON.stringify(req.query), controller);
  // await serviceUtil.checkPermission(req, res, "View", controller);
  const query = await serviceUtil.generateListQuery(req);

  if (query.page === 1)
    // total count
    query.pagination.totalCount = await Plan.totalCount(query);

  //get plan records
  const plans = await Plan.list(query);
  logger.info('Log:plan Controller:list:' + i18nUtil.getI18nMessage('recordsFound'), controller);
  responseJson.respCode = respUtil.getDetailsSuccessResponse().respCode;
  responseJson.plans = plans;
  responseJson.pagination = query.pagination;
  return res.json(responseJson);
}


/**
 * Update existing plan
 * @param req
 * @param res
 * @param next
 * @returns { respCode: respCode, respMessage: respMessage }
 */
async function update(req, res, next) {
  req.query = await serviceUtil.generateListQuery(req);
  logger.info('Log:Plan Controller:update: body :' + JSON.stringify(req.body), controller);

  // await serviceUtil.checkPermission(req, res, "Edit", controller);
  let plan = req.plan;
  plan = Object.assign(plan, req.body);
  plan = await planService.setUpdatePlanVariables(req, plan)
  req.plan = await Plan.save(plan);
  req.entityType = 'plan';
  req.activityKey = 'planUpdate';
  activityService.insertActivity(req);
  logger.info('Log:plan Controller:update:' + i18nUtil.getI18nMessage('planUpdate'), controller);
  return res.json(respUtil.updateSuccessResponse(req));
}

/**
 * Delete plan.
 * @param req
 * @param res
 * @param next
 * @returns { respCode: respCode, respMessage: respMessage }
 */
async function remove(req, res, next) {
  logger.info('Log:Plan Controller:remove: query :' + JSON.stringify(req.query), controller);

  // await serviceUtil.checkPermission(req, res, "Edit", controller);
  let plan = req.plan;
  plan = await planService.setUpdatePlanVariables(req, plan)
  plan.active = false;
  req.plan = await Plan.save(plan);
  req.entityType = 'plan';
  req.activityKey = 'planDelete';
  activityService.insertActivity(req);
  logger.info('Log:plan Controller:remove:' + i18nUtil.getI18nMessage('planDelete'), controller);
  res.json(respUtil.removeSuccessResponse(req));
}

export default {
  create,
  list,
  get,
  load,
  update,
  remove
};
