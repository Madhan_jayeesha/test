import mongoose from 'mongoose';
import Plan from '../models/plan.model';
import User from '../models/user.model';
import UserPlan from '../models/userPlan.model';

import i18nUtil from '../utils/i18n.util';
import respUtil from '../utils/resp.util';

import UserPlanService from '../services/userPlan.service';
import PaypalService from '../services/paypal.service';
import PaymentService from '../services/payment.service';
const paymentService = new PaymentService();
const userPlanService = new UserPlanService();

const controller = 'Payment';
/**
 * Create new user
 * @param req
 * @param res
 * @returns { respCode: respCode, respMessage: respMessage }
 */
async function create(req, res) {
  logger.info('Log:Payment Controller:create: body :' + JSON.stringify(req.body), controller);
  if (req.body.paymentFrom == 'paypal') {
    let data = req.body;
    if (!data.amount) {
      let plan = await Plan.findOne({ planId: req.body.planId });
      if (!plan) {
        req.i18nKey = 'InvalidPlanId';
        return res.json(respUtil.getErrorResponse(req))
      }
      data.amount = plan.price;
    }
    let result = await PaypalService.createPayment(data);
    if (result && result.link) {
      console.log(result);
      res.redirect(result.link);
    }
  }
}

const execute = async (req, res) => {
  let data = {}
  if (req.query.amount) {
    data.amount = req.query.amount;
  }
  let plan = await Plan.findOne({ planId: req.query.planId });
  if (!plan) {
    req.i18nKey = 'InvalidPlanId';
    return res.json(respUtil.getErrorResponse(req));
  }
  data.plan = plan;
  let user = await User.findOne({ _id: mongoose.Types.ObjectId(req.query.userId) });
  if (!user) {
    req.i18nKey = 'InvalidUserId';
    return res.json(respUtil.getErrorResponse(req))
  }
  data.user = user;
  let result = await PaypalService.paymentExecution(req, data);
  if (result && result.details) {
    data.paymentResponse = result.details;
    req.payment = await paymentService.createPaymentRecord(req, data);
    userPlanService.createUserPlan(req, data)
    res.send(result)
  }
}


export default {
  create,
  execute
}