import SecurityQuestion from '../models/securityQuestions.model';
import User from '../models/user.model';
import activityService from '../services/activity.service';

import i18nUtil from '../utils/i18n.util';
import respUtil from '../utils/resp.util';
import serviceUtil from '../utils/service.util';
import sessionUtil from '../utils/session.util';

const controller = "SecurityQuetion";

/**
 * Load securityQuestion and append to req.
 * @param req
 * @param res
 * @param next
 */
async function load(req, res, next) {
  req.securityQuestion = await SecurityQuestion.get(req.params.securityQuestionId);
  return next();
}

/**
 * Get securityQuestion
 * @param req
 * @param res
 * @returns {details: securityQuestion}
 */

async function get(req, res) {
  logger.info('Log:securityQuestion Controller:get: query :' + JSON.stringify(req.query), controller);

  await serviceUtil.checkPermission(req, res, "View", controller);
  req.query = await serviceUtil.generateListQuery(req);
  let securityQuestion = req.securityQuestion;
  logger.info('Log:securityQuestion Controller:get:' + i18nUtil.getI18nMessage('recordFound'), controller);
  let responseJson = {
    respCode: respUtil.getDetailsSuccessResponse().respCode,
    details: securityQuestion
  };
  return res.json(responseJson);
}
/**
 * Create new securityQuestion
 * @param req
 * @param res
 * @returns { respCode: respCode, respMessage: respMessage }
 */
async function create(req, res) {
  logger.info('Log:securityQuestion Controller:create: body :' + JSON.stringify(req.body), controller);

  await serviceUtil.checkPermission(req, res, "Edit", controller);
  let securityQuestion = new SecurityQuestion(req.body);
  req.securityQuestion = await SecurityQuestion.save(securityQuestion);
  req.entityType = 'securityQuestion';
  req.activityKey = 'securityQuestionCreate';
  activityService.insertActivity(req);
  logger.info('Log:securityQuestion Controller:create:' + i18nUtil.getI18nMessage('securityQuestionCreate'), controller);
  return res.json(respUtil.createSuccessResponse(req));
}

/**
 * Update existing securityQuestion
 * @param req
 * @param res
 * @param next
 * @returns { respCode: respCode, respMessage: respMessage }
 */
async function update(req, res, next) {
  logger.info('Log:securityQuestion Controller:update: body :' + JSON.stringify(req.body), controller);

  await serviceUtil.checkPermission(req, res, "Edit", controller);
  let securityQuestion = req.securityQuestion;
  securityQuestion = Object.assign(securityQuestion, req.body);
  req.securityQuestion = await SecurityQuestion.save(securityQuestion);
  req.entityType = 'securityQuestion';
  req.activityKey = 'securityQuestionUpdate';
  activityService.insertActivity(req);
  logger.info('Log:securityQuestion Controller:update:' + i18nUtil.getI18nMessage('securityQuestionUpdate'), controller);
  return res.json(respUtil.updateSuccessResponse(req));
}

/**
 * Get securityQuestion list. based on criteria
 * @param req
 * @param res
 * @param next
 * @returns {securityQuestion: securityQuestion, pagination: pagination}
 */
async function list(req, res, next) {
  logger.info('Log:securityQuestion Controller:list: query :' + JSON.stringify(req.query), controller);

  await serviceUtil.checkPermission(req, res, "View", controller);
  let responseJson = {};
  const query = await serviceUtil.generateListQuery(req);
  //get total securityQuestion
  if (query.page === 1)
    //  total count;
    query.pagination.totalCount = await SecurityQuestion.totalCount(query);

  req.securityQuestions = await SecurityQuestion.list(query);
  responseJson.pagination = query.pagination;
  logger.info('Log:securityQuestion Controller:list:' + i18nUtil.getI18nMessage('recordsFound'), controller);
  responseJson.respCode = respUtil.getDetailsSuccessResponse().respCode;
  responseJson.securityQuestions = req.securityQuestions;
  return res.json(responseJson)
}

/**
 * Delete securityQuestion.
 * @param req
 * @param res
 * @param next
 * @returns { respCode: respCode, respMessage: respMessage }
 */
async function remove(req, res, next) {
  logger.info('Log:securityQuestion Controller:remove: query :' + JSON.stringify(req.query), controller);
  await serviceUtil.checkPermission(req, res, "Edit", controller);
  const securityQuestion = req.securityQuestion;
  securityQuestion.active = false;
  req.securityQuestion = await SecurityQuestion.save(securityQuestion);
  req.entityType = 'securityQuestion';
  req.activityKey = 'securityQuestionDelete';
  activityService.insertActivity(req);
  logger.info('Log:securityQuestion Controller:remove:' + i18nUtil.getI18nMessage('securityQuestionDelete'), controller);
  return res.json(respUtil.removeSuccessResponse(req));
}


/**
 * Save securityQuestion to users
 * @param req
 * @param res
 * @returns { respCode: respCode, respMessage: respMessage }
 */
async function userSecurityQuestions(req, res) {

  await serviceUtil.checkPermission(req, res, "View", controller);
  let user = await User.get(sessionUtil.getTokenInfo(req, '_id'));
  user.securityQuestions = req.body.securityQuestions;
  user.authenticationType = 'securityQuestion';
  await User.save(user);
  req.i18nKey = 'securityQuestionAdded';
  return res.json(respUtil.successResponse(req));
}


export default {
  load,
  get,
  create,
  update,
  list,
  remove,
  userSecurityQuestions
};