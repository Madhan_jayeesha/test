import Employee from '../models/employee.model';

import activityService from '../services/activity.service';
import EmailService from '../services/email.service';
import EmployeeService from '../services/employee.service';

import i18nUtil from '../utils/i18n.util';
import respUtil from '../utils/resp.util';
import serviceUtil from '../utils/service.util';
import sessionUtil from '../utils/session.util';

import Sms from '../services/sms.service';

const employeeService = new EmployeeService();
const emailService = new EmailService();
const smsService = new Sms();
const controller = "Employee";

/**
 * Load Employee and append to req.
 * @param req
 * @param res
 * @param next
 */

async function load(req, res, next) {
  req.employee = await Employee.get(req.params.employeeId);
  return next();
}

/**
 * Get Employee
 * @param req
 * @param res
 * @returns {details: employee}
 */

async function get(req, res) {
  logger.info('Log:Employee Controller:get: query :' + JSON.stringify(req.query), controller);

  await serviceUtil.checkPermission(req, res, "View", controller);
  req.query = await serviceUtil.generateListQuery(req);
  let employee = req.employee;
  logger.info('Log:employee Controller:get:' + i18nUtil.getI18nMessage('recordFound'), controller);
  let responseJson = {
    respCode: respUtil.getDetailsSuccessResponse().respCode,
    details: employee
  };
  return res.json(responseJson);
}

/**
 * Create new employee
 * @param req
 * @param res
 * @returns { respCode: respCode, respMessage: respMessage }
 */
async function create(req, res) {
  logger.info('Log:Employee Controller:create: body :' + JSON.stringify(req.body), controller);

  await serviceUtil.checkPermission(req, res, "Edit", controller);
  let employee = new Employee(req.body);

  //check email exists or not
  const uniqueEmail = await Employee.findUniqueEmail(employee.email);
  if (uniqueEmail) {
    req.i18nKey = 'emailExists';
    logger.error('Error:employee Controller:create:' + i18nUtil.getI18nMessage('emailExists'), controller);
    return res.json(respUtil.getErrorResponse(req));
  }
  employee = await employeeService.setCreateEmployeeVariables(req, employee)
  req.employee = await Employee.save(employee);
  req.employee.password = req.employee.salt = undefined;
  req.entityType = 'employee';
  req.activityKey = 'employeeCreate';
  req.enEmail = serviceUtil.encodeString(req.employee.email);
  activityService.insertActivity(req);
  //send email to employee
  // emailService.sendEmail(req, res);
  // let templateInfo = JSON.parse(JSON.stringify(config.mailSettings));
  // emailService.sendEmailviaGrid({
  //   templateName: config.emailTemplates.employeeWelcome,
  //   emailParams: {
  //     to: employee.email,
  //     displayName: employee.displayName,
  //     Id: req.employee._id,
  //     link: templateInfo.adminUrl
  //   }
  // });
  logger.info('Log:employee Controller:create:' + i18nUtil.getI18nMessage('employeeCreate'), controller);
  return res.json(respUtil.createSuccessResponse(req));
}

/**
* Get employee list. based on criteria
* @param req
* @param res
* @param next
* @returns {employees: employees, pagination: pagination}
*/
async function list(req, res, next) {
  let responseJson = {};
  logger.info('log:Employee Controller:list:query :' + JSON.stringify(req.query), controller);

  await serviceUtil.checkPermission(req, res, "View", controller);
  const query = await serviceUtil.generateListQuery(req);
  //get employee records
  if (query.page === 1)
    // total count
    query.pagination.totalCount = await Employee.totalCount(query);

  const employees = await Employee.list(query);
  logger.info('Log:employee Controller:list:' + i18nUtil.getI18nMessage('recordsFound'), controller);
  responseJson.respCode = respUtil.getDetailsSuccessResponse().respCode;
  responseJson.employees = employees;
  responseJson.pagination = query.pagination;
  return res.json(responseJson);
}


/**
 * Update existing employee
 * @param req
 * @param res
 * @param next
 * @returns { respCode: respCode, respMessage: respMessage }
 */
async function update(req, res, next) {
  req.query = await serviceUtil.generateListQuery(req);
  logger.info('Log:Employee Controller:update: body :' + JSON.stringify(req.body), controller);

  await serviceUtil.checkPermission(req, res, "Edit", controller);
  let employee = req.employee;
  // check unique email
  if (req.body.email && employee.email !== req.body.email) {
    req.i18nKey = 'emailCannotChange';
    logger.error('Error:employee Controller:update:' + i18nUtil.getI18nMessage('emailExists'), controller);
    return res.json(respUtil.getErrorResponse(req));
  }

  employee = Object.assign(employee, req.body);
  employee = await employeeService.setUpdateEmployeeVariables(req, employee)
  req.employee = await Employee.save(employee);
  req.entityType = 'employee';
  req.activityKey = 'employeeUpdate';
  activityService.insertActivity(req);
  logger.info('Log:employee Controller:update:' + i18nUtil.getI18nMessage('employeeUpdate'), controller);
  return res.json(respUtil.updateSuccessResponse(req));
}

/**
 * Delete employee.
 * @param req
 * @param res
 * @param next
 * @returns { respCode: respCode, respMessage: respMessage }
 */
async function remove(req, res, next) {
  logger.info('Log:Employee Controller:remove: query :' + JSON.stringify(req.query), controller);

  await serviceUtil.checkPermission(req, res, "Edit", controller);
  let employee = req.employee;
  employee = await employeeService.setUpdateEmployeeVariables(req, employee)
  employee.active = false;
  req.employee = await Employee.save(employee);
  req.entityType = 'employee';
  req.activityKey = 'employeeDelete';
  activityService.insertActivity(req);
  logger.info('Log:employee Controller:remove:' + i18nUtil.getI18nMessage('employeeDelete'), controller);
  res.json(respUtil.removeSuccessResponse(req));
}


export default {
  create,
  list,
  get,
  load,
  update,
  remove,
};
