import dayjs from 'dayjs';
import utc from 'dayjs/plugin/utc';
// var utc = require('dayjs/plugin/utc');
const defaultFormat = 'YYYY-MM-DD';
const defaultYearFormat = 'YYYY';
const defaultDateTimeFormat = 'YYYY-MM-DDTHH:mm:ss';
dayjs.extend(utc);

const getTodayDate = (format = defaultFormat) => {
  return dayjs.utc().format(format)
}

const getTodayDateAndTime = () => {
  return dayjs.utc().format(defaultDateTimeFormat);
}

const getYesterdayDate = (format = defaultFormat) => {
  return dayjs.utc().add(-1, 'day').format(format);
}
const getWeekBeforeDate = (format = defaultFormat) => {
  return dayjs.utc().add(-1, 'week').format(format);
}

const formatDate = (date, format = defaultFormat) => {
  return dayjs(date).format(format)
}

const getFutureDate = (days, day) => {
  let date = dayjs().add(days, 'days').format(defaultDateTimeFormat);
  if (day)
    date = dayjs(new Date(day)).add(days, 'days').format(defaultDateTimeFormat);
  return date;
}

const formatYear = (date) => {
  let year = dayjs().format(defaultFormat);
  if (year)
    year = dayjs(new Date(date)).format(defaultYearFormat);
  return year;
}

const getOneDayQuery = () => {
  let todayDate = dayjs(new Date()).format(defaultFormat);
  return { $lte: new Date(todayDate + 'T23:59:59Z'), $gte: new Date(todayDate + 'T00:00:00Z') };
}
const getThisWeekQuery = () => {
  let date = new Date();
  let y = date.getFullYear();
  let m = date.getMonth();
  let d = date.getDate();
  let firstDay = dayjs(new Date(y, m, d)).format('YYYY-MM-DD') + 'T23:59:59Z';
  let lastDay = dayjs(new Date(y, m, d - 7)).format('YYYY-MM-DD') + 'T00:00:00Z';
  return { $lte: new Date(firstDay), $gte: new Date(lastDay) };
}

const getThisMonthDatesQuery = () => {
  let date = new Date();
  let y = date.getFullYear();
  let m = date.getMonth();
  let firstDay = dayjs(new Date(y, m, 1)).format('YYYY-MM-DD') + 'T00:00:00Z';
  let lastDay = dayjs(new Date(y, m + 1, 0)).format('YYYY-MM-DD') + 'T00:00:00Z';
  return { $lte: new Date(lastDay), $gte: new Date(firstDay) };
}

const getOneMonthDatesQuery = () => {
  let date = new Date();
  let y = date.getFullYear();
  let m = date.getMonth();
  let d = date.getDate();
  let firstDay = dayjs(new Date(y, m, d)).format('YYYY-MM-DD') + 'T23:59:59Z';
  let lastDay = dayjs(new Date(y, m, d - 30)).format('YYYY-MM-DD') + 'T00:00:00Z';
  return { $lte: new Date(firstDay), $gte: new Date(lastDay) };
}

const getLastMonthDatesQuery = () => {
  let date = new Date();
  let y = date.getFullYear();
  let m = date.getMonth();
  let firstDay = dayjs(new Date(y, m - 1, 1)).format('YYYY-MM-DD') + 'T00:00:00Z';
  let lastDay = dayjs(new Date(y, m, 0)).format('YYYY-MM-DD') + 'T00:00:00Z';
  return { $lte: new Date(firstDay), $gte: new Date(lastDay) };
}

const getThreeMonthsQuery = () => {
  let date = new Date();
  let y = date.getFullYear();
  let m = date.getMonth();
  let d = date.getDate();
  let firstDay = dayjs(new Date(y, m, d)).format('YYYY-MM-DD') + 'T23:59:59Z';
  let lastDay = dayjs(new Date(y, m, d - 90)).format('YYYY-MM-DD') + 'T00:00:00Z';
  return { $lte: new Date(firstDay), $gte: new Date(lastDay) };
}

const getLastMinuteQuery = () => {
  let date = new Date();
  let y = date.getFullYear();
  let m = date.getMonth();
  let d = date.getDate();
  let h = date.getHours();
  let mnts = date.getMinutes();
  let seconds = date.getSeconds();
  let newSeconds = seconds - 60;
  let presentMinuteDate = dayjs(new Date(y, m, d, h, mnts, seconds)).toISOString();
  let lastMinuteDate = dayjs(new Date(y, m, d, h, mnts - 1, Math.abs(newSeconds))).toISOString();
  return { $gte: new Date(lastMinuteDate), $lt: new Date(presentMinuteDate) };
}

const getLast60MinutesQuery = () => {
  let date = new Date();
  let y = date.getFullYear();
  let m = date.getMonth();
  let d = date.getDate();
  let h = date.getHours();
  let mnts = date.getMinutes();
  let seconds = date.getSeconds();
  let newMnts = mnts - 60;
  let presentMinuteDate = dayjs(new Date(y, m, d, h, mnts, seconds)).toISOString();
  let last60MinuteDate = dayjs(new Date(y, m, d, h - 1, Math.abs(newMnts), seconds)).toISOString();
  return { $gte: new Date(last60MinuteDate), $lt: new Date(presentMinuteDate) };
}

const getLastHourQuery = () => {
  let date = new Date();
  let y = date.getFullYear();
  let m = date.getMonth();
  let d = date.getDate();
  let h = date.getHours();
  let mnts = date.getMinutes();
  let seconds = date.getSeconds();
  let presentMinuteDate = dayjs(new Date(y, m, d, h, mnts, seconds)).toISOString();
  let lastMinuteDate = dayjs(new Date(y, m, d, h - 1, mnts, seconds)).toISOString();
  return { $gte: new Date(lastMinuteDate), $lt: new Date(presentMinuteDate) };
}


const getLast24HoursQuery = () => {
  let date = new Date();
  let y = date.getFullYear();
  let m = date.getMonth();
  let d = date.getDate();
  let h = date.getHours();
  let mnts = date.getMinutes();
  let seconds = date.getSeconds();
  let newHour = h - 24;
  let presentDate = dayjs(new Date(y, m, d, h, mnts, seconds)).toISOString();
  let lastDate = dayjs(new Date(y, m, d - 1, Math.abs(newHour), mnts, seconds)).toISOString();
  return { $gte: new Date(lastDate), $lt: new Date(presentDate) };
}

const getOrderExpiryDateQuery = (diffDays) => {
  let date = new Date();
  let y = date.getFullYear();
  let m = date.getMonth();
  let d = date.getDate();
  let h = date.getHours();
  let mnts = date.getMinutes();
  let seconds = date.getSeconds();
  let expDate = dayjs(new Date(y, m, d - diffDays, h, mnts, seconds)).toISOString();
  return { $lt: new Date(expDate) };
}
console.log(getOrderExpiryDateQuery(5));
const getFuturDateByMonth = (date, n) => {
  var date = new Date(date);
  let y = date.getFullYear();
  let m = date.getMonth();
  let d = date.getDate();
  return dayjs(new Date(y, m + n, d)).format(defaultFormat) + 'T00:00:00Z';
}

export default {
  getTodayDate,
  getTodayDateAndTime,
  getYesterdayDate,
  getFutureDate,
  formatDate,
  formatYear,
  getOneDayQuery,
  getThisWeekQuery,
  getThisMonthDatesQuery,
  getLastMonthDatesQuery,
  getThreeMonthsQuery,
  getWeekBeforeDate,
  getOneMonthDatesQuery,
  getLastMinuteQuery,
  getLast24HoursQuery,
  getOrderExpiryDateQuery,
  getLast60MinutesQuery,
  getLastHourQuery,
  getFuturDateByMonth
};