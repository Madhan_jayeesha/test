import Promise from 'bluebird';
import httpStatus from 'http-status';
import mongoose from 'mongoose';

import APIError from '../helpers/APIError';

const Schema = mongoose.Schema;

const DropDownSchema = new mongoose.Schema({
  name: String,
  options: [{
    title: String,
    option: String,
    value: String,
    desc: String,
    ispremium: { type: Boolean, default: false },
  }],
  status: { type: String, default: "Active" },
  active: { type: Boolean, default: true },
  created: { type: Date, default: Date.now },

  createdBy: {
    employee: { type: Schema.ObjectId, ref: 'Employee' },
    name: String
  },
  updatedBy: {
    employee: { type: Schema.ObjectId, ref: 'Employee' },
    name: String
  }
}, { usePushEach: true });

/**
 * statics
 */

DropDownSchema.statics = {

  /**
   * save and update dropDown
   * @param dropDown
   * @returns {Promise<dropDown, APIError>}
   */

  save(dropDown) {
    return dropDown.save()
      .then((dropDown) => {
        if (dropDown) {
          return dropDown;
        }
        const err = new APIError('error in dropDown', httpStatus.NOT_FOUND);
        return Promise.reject(err);
      });

  },

  /**
   * List dropDown in descending order of 'createdAt' timestamp.
   * @returns {Promise<dropDown[]>}
   */

  list(query) {
    return this.find(query.filter)
      .populate('createdBy.employee', 'displayName photo')
      .populate('updatedBy.employee', 'displayName photo')
      .populate('attachment.employee', 'firstName lastName displayName photo')
      .sort(query.sorting)
      .skip((query.page - 1) * query.limit)
      .limit(query.limit)
      .exec();
  },

  /**
   * Count of dropDown records
   * @returns {Promise<dropDown[]>}
   */

  totalCount(query) {
    return this.find(query.filter)
      .count();
  },

  /**
   * Get dropDown
   * @param {ObjectId} id - The objectId of dropDown.
   * @returns {Promise<dropDown, APIError>}
   */

  get(id) {
    return this.findById(id)
      .populate('createdBy.employee', 'displayName photo')
      .populate('updatedBy.employee', 'displayName photo')
      .populate('attachment.employee', 'firstName lastName displayName photo')
      .exec()
      .then((dropDown) => {
        if (dropDown) {
          return dropDown;
        }
        const err = new APIError('No such dropDown exists', httpStatus.NOT_FOUND);
        return Promise.reject(err);
      })
  }

}

export default mongoose.model('DropDown', DropDownSchema);