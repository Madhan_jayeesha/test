import Promise from 'bluebird';
import httpStatus from 'http-status';
import mongoose from 'mongoose';

import APIError from '../helpers/APIError';

const Schema = mongoose.Schema;


/**
 * payment Schema
 */
const PaymentSchema = new mongoose.Schema({

  amount: Number,
  currency: String,
  paymentFrom: String,
  response: Object,
  updatedBy: {
    user: { type: Schema.ObjectId, ref: 'User' },
  },
  createdBy: {
    user: { type: Schema.ObjectId, ref: 'User' },
  },
  created: { type: Date, default: Date.now },
  active: { type: Boolean, default: true },
  updated: { type: Date, default: Date.now },
  paymentId: String
}, { usePushEach: true });

/**
 * Statics
 */
PaymentSchema.statics = {
  /**
   * save and update payment
   * @param payment
   * @returns {Promise<Payment, APIError>}
   */
  save(payment) {
    return payment.save()
      .then((payment) => {
        if (payment) {
          return payment;
        }
        const err = new APIError('Error in payment', httpStatus.NOT_FOUND);
        return Promise.reject(err);
      });
  },

  /**
   * Get payment
   * @param {ObjectId} id - The objectId of payment.
   * @returns {Promise<Payment, APIError>}
   */
  get(id) {
    return this.findById(id)
      .then((payment) => {
        if (payment) {
          return payment;
        }
        const err = new APIError('No such payment exists!', httpStatus.NOT_FOUND);
        return Promise.reject(err);
      });
  },

  /**
   * List payment in descending order of 'createdAt' timestamp.
   * @returns {Promise<Payment[]>}
   */
  list(query) {
    return this.find(query.filter)
      .sort(query.sorting)
      .skip((query.page - 1) * query.limit)
      .limit(query.limit)
      .exec();
  },

  /**
   * Count of payment records
   * @returns {Promise<Payment[]>}
   */
  totalCount(query) {
    return this.find(query.filter)
      .countDocuments();
  }

};

/**
 * @typedef Payment
 */
export default mongoose.model('Payment', PaymentSchema);
