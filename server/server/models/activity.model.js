import Promise from 'bluebird';
import mongoose from 'mongoose';
import httpStatus from 'http-status';

import APIError from '../helpers/APIError';
const Schema = mongoose.Schema;
/**
 * Activity Schema
 */
const ActivitySchema = new mongoose.Schema({
  contextId: Schema.ObjectId,
  contextType: String,
  context: String,
  desc: String,
  value: String,
  type: String,
  name: String,
  key: String,
  description: Array,
  pair: String,
  currencySymbol: String,
  price: Number,
  activityType: String,
  userName: String,
  ipAdress: String,
  location: Object,
  country: String,
  device: String,
  email: String,
  requestJson: Object,
  loginFrom: String,
  loginType: String,

  userId: { type: Schema.ObjectId, ref: 'User' },
  employeeId: { type: Schema.ObjectId, ref: 'Employee' },
  userObjId: { type: Schema.ObjectId, ref: 'User' },
  created: { type: Date, default: Date.now },
  active: { type: Boolean, default: true },

  createdBy: {
    employee: { type: Schema.ObjectId, ref: 'Employee' },
    user: { type: Schema.ObjectId, ref: 'User' }
  }
}, { usePushEach: true });

/**
 * Statics
 */
ActivitySchema.statics = {
  /**
   * save and update activity
   * @param activity
   * @returns {Promise<Activity, APIError>}
   */
  save(activity) {
    return activity.save()
      .then((activity) => {
        if (activity) {
          return activity;
        }
        const err = new APIError('Error in activity', httpStatus.NOT_FOUND);
        return Promise.reject(err);
      });
  },

  /**
   * Get activity
   * @param {ObjectId} id - The objectId of activity.
   * @returns {Promise<Activity, APIError>}
   */
  get(id) {
    return this.findById(id)
      .exec()
      .then((activity) => {
        if (activity) {
          return activity;
        }
        const err = new APIError('No such activity exists!', httpStatus.NOT_FOUND);
        return Promise.reject(err);
      });
  },

  /**
   * List activity in descending order of 'createdAt' timestamp.
   * @returns {Promise<Activity[]>}
   */
  list(query) {
    return this.find(query.filter)
      .populate("createdBy.employee", 'firstName lastName displayName')
      .populate("createdBy.user", 'firstname lastname userName')
      .sort(query.sorting)
      .skip((query.page - 1) * query.limit)
      .limit(query.limit)
      .exec();
  },

  historyList(query) {
    return this.find(query.filter, ({ _id: 0, description: 1 }))
      .sort(query.sorting)
      .skip(query.page - 1 * query.limit)
      .limit(query.limit)
      .exec();
  },
  /**
   * Count of activity records
   * @returns {Promise<Activity[]>}
   */
  totalCount(query) {
    return this.find(query.filter)
      .count();
  }

};

/**
 * @typedef Activity
 */
export default mongoose.model('Activity', ActivitySchema);
