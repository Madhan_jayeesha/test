import Promise from 'bluebird';
import httpStatus from 'http-status';
import mongoose from 'mongoose';
import mongooseFloat from 'mongoose-float';

import APIError from '../helpers/APIError';

const Float = mongooseFloat.loadType(mongoose);
const Schema = mongoose.Schema;
const SchemaTypes = mongoose.Schema.Types;

/**
 * Statement Schema
 */
const TicketSchema = new mongoose.Schema({
	updated: Date,
	emailId: String,
	userLastUpdated: Date,
	adminLastUpdated: Date,
	country: String,

	subject: { type: String, default: '' },
	message: { type: String, default: '' },
	created: { type: Date, default: Date.now },
	active: { type: Boolean, default: true },
	category: { type: String, default: '' },
	ticketId: { type: String, default: '' },
	ticketNumber: { type: String, default: '' },
	status: { type: String, default: 'New' },
	userId: { type: String, default: '' },
	userName: { type: String, default: '' },
	assignedTo: { type: Schema.ObjectId, ref: 'Employee' },

	createdBy: {
		user: { type: Schema.ObjectId, ref: 'User' },
		employee: { type: Schema.ObjectId, ref: 'Employee' }
	},
	updatedBy: {
		user: { type: Schema.ObjectId, ref: 'User' },
		employee: { type: Schema.ObjectId, ref: 'Employee' }
	},

	comments: [
		{
			message: String,
			postedBy: {
				user: { type: Schema.Types.ObjectId, ref: 'User' },
				employee: { type: Schema.Types.ObjectId, ref: 'Employee' }
			},
			created: { type: Date, default: Date.now }
		}
	]
}, { usePushEach: true });

TicketSchema.statics = {

	/**
	 * save and update Ticket
	 * @param Ticket
	 * @returns {Promise<Ticket, APIError>}
	 */
	save(ticket) {
		return ticket.save()
			.then((ticket) => {
				if (ticket) {
					return ticket;
				}
				const err = new APIError('error in ticket', httpStatus.NOT_FOUND);
				return Promise.reject(err);
			});

	},

	/**
 * List Ticket in descending order of 'createdAt' timestamp.
 * @returns {Promise<ticket[]>}
 */
	list(query) {
		return this.find(query.filter)
			.sort(query.sorting)
			.populate('createdBy.user', 'firstname lastname userId userName profilePic')
			.populate('createdBy.employee', 'firstName lastName displayName profilePic')
			.populate('assignedTo', 'firstName lastName displayName profilePic')
			.populate('comments.postedBy.user', 'firstname lastname userId userName profilePic')
			.populate('comments.postedBy.employee', 'firstName lastName displayName profilePic')
			.skip((query.page - 1) * query.limit)
			.limit(query.limit)
			.exec();
	},

	/**
 * Count of ticket records
 * @returns {Promise<ticket[]>}
 */
	totalCount(query) {
		return this.find(query.filter)
			.count();
	},

	/**
	 * Get ticket
	 * @param {ObjectId} id - The objectId of ticket.
	 * @returns {Promise<ticket, APIError>}
	 */
	get(id) {
		return this.findById(id)
			.populate('createdBy.user', 'firstname lastname userId userName profilePic')
			.populate('createdBy.employee', 'firstName lastName displayName profilePic')
			.populate('assignedTo', 'firstName lastName displayName profilePic')
			.populate('comments.postedBy.user', 'firstname lastname userId userName profilePic')
			.populate('comments.postedBy.employee', 'firstName lastName displayName profilePic')
			.exec()
			.then((ticket) => {
				if (ticket) {
					return ticket;
				}
				const err = new APIError('No such ticket exists', httpStatus.NOT_FOUND);
				return Promise.reject(err);
			})
	},

	/**
	 * Get LastTicket
	 * @param {ObjectId} id - The objectId of ticket.
	 * @returns {Promise<ticket, APIError>}
	 */
	getLastTicket() {
		return this.findOne({ 'active': true })
			.sort({ 'created': -1 })
			.exec();
	},

	/**
	 * reply to ticket
	 * @param {ObjectId} id - The objectId of ticket.
	 * @returns {Promise<ticket, APIError>}
	 */
	replyTicket(req, newvalues) {
		return this.update({ '_id': req.query.ticketId }, newvalues)
			.exec();
	},

	/**
	 * get tickets count based on users 
	 * @returns {Promise<ticket[]>}
	 */
	getTicketCounts(query) {
		return this.aggregate(query)
			.exec()
			.then((ticket) => {
				if (ticket) {
					return ticket;
				}
				const err = new APIError('No tickets exists!', httpStatus.NOT_FOUND);
				return Promise.reject(err);
			});
	}
}

export default mongoose.model('Ticket', TicketSchema);
