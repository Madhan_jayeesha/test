import Promise from 'bluebird';
import mongoose from 'mongoose';
import httpStatus from 'http-status';

import APIError from '../helpers/APIError';

import errorService from '../services/error.service';

const Schema = mongoose.Schema;
/**
 * Alerts Schema
 */
const AlertsSchema = new mongoose.Schema({
  contextId: Schema.ObjectId,
  contextType: String,
  context: String,
  currency: String,
  email: String,
  comment: String,
  desc: String,
  value: String,
  type: String,
  key: String,
  description: Array,
  pair: String,
  price: String,
  amount: String,
  updated: Date,

  userId: { type: Schema.ObjectId, ref: 'User' },
  employeeId: { type: Schema.ObjectId, ref: 'Employee' },
  created: { type: Date, default: Date.now },
  status: { type: String, default: 'unRead' },
  active: { type: Boolean, default: true },

  createdBy: {
    user: { type: Schema.ObjectId, ref: 'User' },
    employee: { type: Schema.ObjectId, ref: 'Employee' }
  },
  updatedBy: {
    user: { type: Schema.ObjectId, ref: 'User' },
    employee: { type: Schema.ObjectId, ref: 'Employee' }
  }
}, { usePushEach: true });
/**
 * Statics
 */
AlertsSchema.statics = {
  /**
   * save and update alert
   * @param alert
   * @returns {Promise<alert, APIError>}
   */
  save(alert) {
    return alert.save()
      .then((alert) => {
        if (alert) {
          return alert;
        }
        let req = {};
        req.errorKey = 'alertCreateError';
        errorService.insertActivity(req);
        const err = new APIError('Error in user', httpStatus.NOT_FOUND);
        return Promise.reject(err);
      });
  },

  /**
    * List all countries 
    * @returns {Promise<alert[]>}
    */
  list(query) {
    return this.find(query.filter)
      .skip((query.page - 1) * query.limit)
      .limit(query.limit)
      .sort(query.sorting)
      .exec();
  },

  /**
    * Count of alert records
    * @returns {Promise<alert[]>}
    */
  totalCount(query) {
    return this.find(query.filter)
      .count();
  },

  /**
     * Get alert
     * @param {ObjectId} id - The objectId of alert.
     * @returns {Promise<alert, APIError>}
     */
  get(id) {
    return this.findById(id)
      .exec()
      .then((alert) => {
        if (alert) {
          return alert;
        }
        let req = {};
        req.errorKey = 'NoSuchAlertExist';
        errorService.insertActivity(req);
        const err = new APIError('No such alert exists!', httpStatus.NOT_FOUND);
        return Promise.reject(err);
      });
  }
};

/**
 * @typedef Alert
 */
export default mongoose.model('Alert', AlertsSchema);