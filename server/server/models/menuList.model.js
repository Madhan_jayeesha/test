import Promise from 'bluebird';
import httpStatus from 'http-status';
import mongoose from 'mongoose';

import APIError from '../helpers/APIError';

const Schema = mongoose.Schema;

const MenuSchema = new mongoose.Schema({
  label: String,
  displayTitle: String,
  dashboard: String,
  sequenceNo: Number,
  icon: String,
  title: String,
  route: String,
  submenus: Array,
  updated: Date,

  active: { type: Boolean, default: true },
  created: { type: Date, default: Date.now },

  createdBy: {
    employee: { type: Schema.ObjectId, ref: 'Employee' },
    name: String
  },
  updatedBy: {
    employee: { type: Schema.ObjectId, ref: 'Employee' },
    name: String
  }
}, { usePushEach: true });

/**
 * statics
 */

MenuSchema.statics = {

  /**
   * save and update menu
   * @param menu
   * @returns {Promise<menu, APIError>}
   */

  save(menu) {
    return menu.save()
      .then((menu) => {
        if (menu) {
          return menu;
        }
        const err = new APIError('error in menu', httpStatus.NOT_FOUND);
        return Promise.reject(err);
      });

  },

  /**
   * List menu in descending order of 'createdAt' timestamp.
   * @returns {Promise<menu[]>}
   */

  list(query) {
    return this.find(query.filter)
      .populate('createdBy.employee', 'displayName photo')
      .populate('updatedBy.employee', 'displayName photo')
      .populate('attachment.employee', 'firstName lastName displayName photo')
      .sort(query.sorting)
      .skip((query.page - 1) * query.limit)
      .limit(query.limit)
      .exec();
  },

  /**
   * Count of menu records
   * @returns {Promise<menu[]>}
   */

  totalCount(query) {
    return this.find(query.filter)
      .count();
  },

  /**
   * Get menu
   * @param {ObjectId} id - The objectId of menu.
   * @returns {Promise<menu, APIError>}
   */

  get(id) {
    return this.findById(id)
      .populate('createdBy.employee', 'displayName photo')
      .populate('updatedBy.employee', 'displayName photo')
      .populate('attachment.employee', 'firstName lastName displayName photo')
      .exec()
      .then((menu) => {
        if (menu) {
          return menu;
        }
        const err = new APIError('No such menu exists', httpStatus.NOT_FOUND);
        return Promise.reject(err);
      })
  }

}

export default mongoose.model('Menu', MenuSchema);