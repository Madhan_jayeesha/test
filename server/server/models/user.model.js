import Promise from 'bluebird';
import crypto from 'crypto';
import httpStatus from 'http-status';
import mongoose from 'mongoose';
import mongooseFloat from 'mongoose-float';

import APIError from '../helpers/APIError';

import errorService from '../services/error.service';

const Float = mongooseFloat.loadType(mongoose);
const Schema = mongoose.Schema;

/**
 * User Schema
 */
const UserSchema = new mongoose.Schema({
  displayName: String,
  twitterId: String,
  email: String,
  phone: String,
  deviceName: Array,
  disabledDate: Date,
  updated: Date,
  userId: String,
  photo: String,
  loginFrom: String,
  created: { type: Date, default: Date.now },
  status: { type: String, default: 'Active' },
  active: { type: Boolean, default: true },
  updatedBy: {
    employee: { type: Schema.ObjectId, ref: 'Employee' },
  },
}, { usePushEach: true });



UserSchema.statics = {

  /**
   * save and update User
   * @param User
   * @returns {Promise<User, APIError>}
   */
  save(user) {
    return user.save()
      .then((user) => {
        if (user) {
          return user;
        }
        const err = new APIError('error in user', httpStatus.NOT_FOUND);
        return Promise.reject(err);
      });

  },

  /**
 * List user in descending order of 'createdAt' timestamp.
 * @returns {Promise<user[]>}
 */
  list(query) {
    return this.find(query.filter)
      .sort(query.sorting)
      .skip((query.page - 1) * query.limit)
      .limit(query.limit)
      .exec();
  },

  /**
   * Count of user records
   * @returns {Promise<user[]>}
   */
  totalCount(query) {
    return this.find(query.filter)
      .count();
  },
  /**
   * Get user
   * @param {ObjectId} id - The objectId of user.
   * @returns {Promise<user, APIError>}
   */
  get(id) {
    return this.findById(id)
      .exec()
      .then((user) => {
        if (user) {
          return user;
        }
        const err = new APIError('No such user exists', httpStatus.NOT_FOUND);
        return Promise.reject(err);
      })
  },

  /**
   * Find unique email.
   * @param {string} email.
   * @returns {Promise<User[]>}
   */
  findUniqueEmail(email) {
    email = email.toLowerCase();
    return this.findOne({
      email: email,
      active: true
    })
      .exec()
      .then((user) => user);
  }
}

export default mongoose.model('User', UserSchema);
