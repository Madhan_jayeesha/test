import Promise from 'bluebird';
import mongoose from 'mongoose';
import httpStatus from 'http-status';

import APIError from '../helpers/APIError';

const Schema = mongoose.Schema;
/**
 * Notification Schema
 */
const NotificationSchema = new mongoose.Schema({
  userName: String,
  userEmail: String,
  notificationType: String,
  status: String,
  title: String,
  body: String,
  updated: Date,

  userId: { type: Schema.ObjectId, ref: "User" },
  created: { type: Date, default: Date.now },
  isSend: { type: Boolean, default: false },
  active: { type: Boolean, default: true },

  createdBy: {
    user: { type: Schema.ObjectId, ref: 'User' },
    employee: { type: Schema.ObjectId, ref: 'Employee' }
  },
  updatedBy: {
    user: { type: Schema.ObjectId, ref: 'User' },
    employee: { type: Schema.ObjectId, ref: 'Employee' }
  }
}, { usePushEach: true });

/**
 * Statics
 */
NotificationSchema.statics = {
  /**
   * save and update notification
   * @param notification
   * @returns {Promise<Notification, APIError>}
   */
  save(notification) {
    return notification.save()
      .then((notification) => {
        if (notification) {
          return notification;
        }
        const err = new APIError('Error in notification', httpStatus.NOT_FOUND);
        return Promise.reject(err);
      });
  },

  /**
   * Get notification
   * @param {ObjectId} id - The objectId of notification.
   * @returns {Promise<Notification, APIError>}
   */
  get(id) {
    return this.findById(id)
      .populate('createdBy.employee', 'dispalyName')
      .populate('createdBy.user', 'dispalyName')
      .populate('createdBy.hospital', 'dispalyName')
      .populate('createdBy.doctor', 'dispalyName')
      .exec()
      .then((notification) => {
        if (notification) {
          return notification;
        }
        const err = new APIError('No such notification exists!', httpStatus.NOT_FOUND);
        return Promise.reject(err);
      });
  },

  /**
   * List notification in descending order of 'createdAt' timestamp.
   * @returns {Promise<Notification[]>}
   */
  list(query) {
    return this.find(query.filter)
      .populate('createdBy.employee', 'dispalyName')
      .populate('createdBy.user', 'dispalyName')
      .populate('createdBy.hospital', 'dispalyName')
      .populate('createdBy.doctor', 'dispalyName')
      .sort(query.sorting)
      .skip((query.page - 1) * query.limit)
      .limit(query.limit)
      .exec();
  },
  /**
   * Count of notification records
   * @returns {Promise<Notification[]>}
   */
  totalCount(query) {
    return this.find(query.filter)
      .count();
  }

};

/**
 * @typedef Notification
 */
export default mongoose.model('Notification', NotificationSchema);
