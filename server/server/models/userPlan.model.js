import httpStatus from 'http-status';
import mongoose from 'mongoose';
import mongooseFloat from 'mongoose-float';
import Promise from 'bluebird';

import APIError from '../helpers/APIError';

const Float = mongooseFloat.loadType(mongoose);
const Schema = mongoose.Schema;

/**
 * UserPlanSchema Schema
 */
const UserPlanSchema = new mongoose.Schema({
  userName: String,
  userObjId: { type: Schema.ObjectId, ref: "User" },
  email: String,
  planObjId: { type: Schema.ObjectId, ref: "Plan" },
  paymentId: Object,
  planName: String,
  planAmount: Number,
  duration: Number,
  startDate: Date,
  endDate: Date,
  renewalDate: Date,
  active: { type: Boolean, default: true },
  status: { type: String, default: 'Active' },
  created: { type: Date, default: Date.now },
  updated: { type: Date, default: Date.now },
}, {
  usePushEach: true
});


/**
 * Statics
 */
UserPlanSchema.statics = {
  /**
   * save and update UserPlan
   * @param userplan
   * @returns {Promise<User, APIError>}
   */
  save(userplan) {
    return userplan.save()
      .then((userplan) => {
        if (userplan) {
          return userplan;
        }
        const err = new APIError('Error in user', httpStatus.NOT_FOUND);
        return Promise.reject(err);
      });
  },

  /**
   * Get UserPlan
   * @param {ObjectId} id - The objectId of UserPlan.
   * @returns {Promise<User, APIError>}
   */
  get(id) {
    return this.findById(id)
      .exec()
      .then((userplan) => {
        if (userplan) {
          return userplan;
        }
        const err = new APIError('No such user exists!', httpStatus.NOT_FOUND);
        return Promise.reject(err);
      });
  },

  /**
   * List UserPlan in descending order of 'createdAt' timestamp.
   * @returns {Promise<User[]>}
   */
  list(query) {
    return this.find(query.filter)
      .sort(query.sorting)
      .skip((query.page - 1) * query.limit)
      .limit(query.limit)
      .exec();
  },

  /**
   * Count of UserPlan records
   * @returns {Promise<User[]>}
   */
  totalCount(query) {
    return this.find(query.filter)
      .countDocuments();
  }
};


/**
 * @typedef UserPlan
 */
export default mongoose.model('UserPlan', UserPlanSchema);
