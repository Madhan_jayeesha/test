import Promise from 'bluebird';
import httpStatus from 'http-status';
import mongoose from 'mongoose';

import APIError from '../helpers/APIError';

const Schema = mongoose.Schema;


/**
 * plan Schema
 */
const PlanSchema = new mongoose.Schema({
  name: String,
  duration: Number,
  price: Number,
  description: String,
  updatedBy: {
    employee: { type: Schema.ObjectId, ref: 'Employee' },
  },
  createdBy: {
    employee: { type: Schema.ObjectId, ref: 'Employee' },
  },
  created: { type: Date, default: Date.now },
  status: { type: String, default: 'Active' },
  active: { type: Boolean, default: true },
  updated: { type: Date, default: Date.now },
  planId: { type: String }
}, { usePushEach: true });

/**
 * Statics
 */
PlanSchema.statics = {
  /**
   * save and update plan
   * @param plan
   * @returns {Promise<Plan, APIError>}
   */
  save(plan) {
    return plan.save()
      .then((plan) => {
        if (plan) {
          return plan;
        }
        const err = new APIError('Error in plan', httpStatus.NOT_FOUND);
        return Promise.reject(err);
      });
  },

  /**
   * Get plan
   * @param {ObjectId} id - The objectId of plan.
   * @returns {Promise<Plan, APIError>}
   */
  get(id) {
    return this.findById(id)
      .then((plan) => {
        if (plan) {
          return plan;
        }
        const err = new APIError('No such plan exists!', httpStatus.NOT_FOUND);
        return Promise.reject(err);
      });
  },

  /**
   * List plan in descending order of 'createdAt' timestamp.
   * @returns {Promise<Plan[]>}
   */
  list(query) {
    return this.find(query.filter)
      .sort(query.sorting)
      .skip((query.page - 1) * query.limit)
      .limit(query.limit)
      .exec();
  },

  /**
   * Count of plan records
   * @returns {Promise<Plan[]>}
   */
  totalCount(query) {
    return this.find(query.filter)
      .countDocuments();
  }

};

/**
 * @typedef Plan
 */
export default mongoose.model('Plan', PlanSchema);
