import Promise from 'bluebird';
import mongoose from 'mongoose';
import httpStatus from 'http-status';

import APIError from '../helpers/APIError';

import errorService from '../services/error.service';

const Schema = mongoose.Schema;

/**
 * SecurityQuestions Schema
 */
const SecurityQuestionsSchema = new mongoose.Schema({
  question: String,
  updated: Date,

  userId: { type: Schema.ObjectId, ref: 'User' },
  created: { type: Date, default: Date.now },
  active: { type: Boolean, default: true },

  createdBy: {
    employee: { type: Schema.ObjectId, ref: 'Employee' }
  },
  updatedBy: {
    employee: { type: Schema.ObjectId, ref: 'Employee' }
  }
}, { usePushEach: true });
/**
 * Statics
 */
SecurityQuestionsSchema.statics = {
  /**
   * save and update securityQuestion
   * @param securityQuestion
   * @returns {Promise<securityQuestion, APIError>}
   */
  save(securityQuestion) {
    return securityQuestion.save()
      .then((securityQuestion) => {
        if (securityQuestion) {
          return securityQuestion;
        }
        let req = {};
        req.errorKey = 'securityQuestionCreateError';
        errorService.insertActivity(req);
        const err = new APIError('Error in user', httpStatus.NOT_FOUND);
        return Promise.reject(err);
      });
  },

  /**
    * List all countries 
    * @returns {Promise<securityQuestion[]>}
    */
  list(query) {
    return this.find(query.filter)
      .skip((query.page - 1) * query.limit)
      .limit(query.limit)
      .sort(query.sorting)
      .exec();
  },

  /**
    * Count of securityQuestion records
    * @returns {Promise<securityQuestion[]>}
    */
  totalCount(query) {
    return this.find(query.filter)
      .count();
  },

  /**
     * Get securityQuestion
     * @param {ObjectId} id - The objectId of securityQuestion.
     * @returns {Promise<securityQuestion, APIError>}
     */
  get(id) {
    return this.findById(id)
      .exec()
      .then((securityQuestion) => {
        if (securityQuestion) {
          return securityQuestion;
        }
        let req = {};
        req.errorKey = 'NoSuchSecurityQuestionExist';
        errorService.insertActivity(req);
        const err = new APIError('No such securityQuestion exists!', httpStatus.NOT_FOUND);
        return Promise.reject(err);
      });
  }
};

/**
 * @typedef SecurityQuestion
 */
export default mongoose.model('SecurityQuestions', SecurityQuestionsSchema);