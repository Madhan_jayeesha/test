#!/bin/bash
VALID_ARGS="major|minor|patch|dev"
# GIT_BRANCH=#this needs to be grabbed
# The git tag regex might need to chang to something like
#  "v[0-9]+\.[0-9]+\.[0-9]+(-$GIT_BRANCH\.[0-9]+)?"
GIT_TAG=$(git tag | grep -E "^v[0-9]+\.[0-9]+\.[0-9]+$"  | sort -V -r | head -n 1)

if [ $1 = "dev" ]; then
    GIT_BRANCH=$(git branch | sed -n '/\* /s///p')
    GIT_TAG=$(git tag | grep -E "^v[0-9]+\.[0-9]+\.[0-9]+\-$GIT_BRANCH\.[0-9]+$" | sort -V -r | head -n 1)
fi

VERSION_FROM_TAG=${GIT_TAG//v}
IFS=. read -r v1 v2 v3 <<< "${VERSION_FROM_TAG}"    # split into (integer) components
if [[ -z $1 ]]; then
  echo "Must provide an argument: $VALID_ARGS"
  exit 1
fi

# Genterating a NEW_VERSION
if [[ ! $1 =~ (major|minor|patch|dev) ]]; then
  echo "$1 is an invalid argument"
  echo "expected: $VALID_ARGS"
  exit 1
elif [ $1 = "major" ]; then
  NEW_VERSION="$((++v1)).0.0"
elif [ $1 = "minor" ]; then
  NEW_VERSION="${v1}.$((++v2)).0"
elif [ $1 = "patch" ]; then
  NEW_VERSION="${v1}.${v2}.$((++v3))"
elif [ $1 = "dev" ]; then
      # This needs to be updated
      # There might need to be two elifs
      # one for if $1 is dev and the GIT_TAG has the GIT_BRANCH in it
      # and one for if $1 is dev and the GIT_TAG doesn't have the GIT_BRANCH in it
    if [[  -z $GIT_TAG  ]]; then
        cd server/
        CURRENT_VERSION=$(node -p "require('./package.json').version")
        IFS=. read -r v1 v2 v3 <<< "${CURRENT_VERSION}"
        NEW_VERSION="${v1}.$((++v2)).0-$GIT_BRANCH.0"
        cd ../
    else 
        VERSION_FROM_TAG=${VERSION_FROM_TAG//-$GIT_BRANCH}
        echo $VERSION_FROM_TAG "----VERSION"
        IFS=. read -r v1 v2 v3 v4 <<< "${VERSION_FROM_TAG}"
        NEW_VERSION="${v1}.${v2}.${v3}-$GIT_BRANCH.$((++v4))"
    fi
fi

NEW_TAG="v$NEW_VERSION"
echo $NEW_TAG

GIT_FETCH=$(git fetch)
GIT_STATUS=$(git status)
if [[ ! "$GIT_STATUS" =~  "Your branch is up to date" ]]; then
  echo "Remote has new changes:"
  echo "$GIT_STATUS"
  exit 1
fi

for DIR in server client admin
do
  cd "$DIR/"
  echo "updating $DIR $(node -p "require('./package.json').version") to $NEW_VERSION"
  ERROR=$(sed -i '' '1,3 s/\"version\":.*/\"version\": "'${NEW_VERSION}'",/' "package.json" || echo "ERROR")
  ERROR1=$(sed -i '' '1,3 s/\"version\":.*/\"version\": "'${NEW_VERSION}'",/' "package-lock.json" || echo "ERROR")
  # this following command is attempting to select only the ffirest "version": of a given file, it doesn't work yet
  # sed -i '1,3/"version":.*/"version": "0.0.0",/'
  cd "../"
done
if [[ ! ($ERROR = "ERROR"  || $ERROR1 = "ERROR") ]]; then
  # These are currently just echo commands so you can see what would happen, once
  # everything seems to work the text echo and the double quotes can be removed
  git add */package*
  git commit -m $NEW_VERSION
  git tag $NEW_TAG
else
  echo "Error occurred:"
  echo "$ERROR"
fi
