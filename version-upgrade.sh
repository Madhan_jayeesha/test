#!/bin/bash
GIT_RESULT=$(git pull)
echo  "GIT_RESULT-----   "$GIT_RESULT
if [ "$GIT_RESULT" =  "Already up to date." ]; then

   # dev
   if [ "$1" = "dev" ]; then
      echo
      echo "Updating server package.json version for dev"
      echo
      cd server/
      NEW_VERSION="dev-"
      NEW_VERSION+=`date +%Y.%m.%d-%H.%M.%Z`
      CURRENT_VERSION=$(node -p "require('./package.json').version") 

      echo "CURRENT_VERSION:  "$CURRENT_VERSION
      echo

      echo "NEW_VERSION:  "$NEW_VERSION
      echo
      # replacing version in package.json with NEW_VERSION
      sed -i '' 's/\"version\":.*/\"version\": "'${NEW_VERSION}'",/' "package.json"

      echo "Updating admin package.json version for dev"
      echo
      cd ../admin/
      CURRENT_VERSION=$(node -p "require('./package.json').version") 
      echo "CURRENT_VERSION:  "$CURRENT_VERSION
      echo

      echo "NEW_VERSION:  "$NEW_VERSION
      echo
      # replacing version in package.json with NEW_VERSION
      sed -i '' 's/\"version\":.*/\"version\": "'${NEW_VERSION}'",/' "package.json"


      echo "Updating client package.json version for dev"
      echo
      cd ../client/
      CURRENT_VERSION=$(node -p "require('./package.json').version") 
      echo "CURRENT_VERSION:  "$CURRENT_VERSION
      echo

      echo "NEW_VERSION:  "$NEW_VERSION
      echo
      # replacing version in package.json with NEW_VERSION
      sed -i '' 's/\"version\":.*/\"version\": "'${NEW_VERSION}'",/' "package.json"

      cd ../
      echo "....Pushing into Git...."
      echo   
      # pushing changes into git with NEW_VERSION as tag
      git add */package*
      git commit -m "$NEW_VERSION"
      git push
      git tag "$NEW_VERSION"


   elif [ "$1" = "major" ]; then
      #  Using npm version major command to update the version 
      echo
      echo "Updating server package.json version for major"
      echo
      cd server/
      CURRENT_VERSION=$(node -p "require('./package.json').version") 
      echo "CURRENT_VERSION:  "$CURRENT_VERSION
      echo
      NEW_VERSION=$(npm version major)
      echo "NEW_VERSION:  "$NEW_VERSION
      echo

      echo "Updating admin package.json version for major"
      echo
      cd ../admin/
      CURRENT_VERSION=$(node -p "require('./package.json').version") 
      echo "CURRENT_VERSION:  "$CURRENT_VERSION
      echo
      NEW_VERSION=$(npm version major)
      echo "NEW_VERSION:  "$NEW_VERSION
      echo

      echo "Updating client package.json version for major" 
      echo 
      cd ../client/
      CURRENT_VERSION=$(node -p "require('./package.json').version") 
      echo "CURRENT_VERSION:  "$CURRENT_VERSION
      echo
      NEW_VERSION=$(npm version major)
      echo "NEW_VERSION:  "$NEW_VERSION
      echo

      echo "....Pushing into Git...."
      echo
      cd ../

      # pushing changes into git with NEW_VERSION as tag
      git add */package*
      git commit -m "$NEW_VERSION"
      git push
      git tag "$NEW_VERSION"

   elif [ "$1" = "patch" ]; then
      #  Using npm version patch command to update the version 
      echo
      echo "Updating server package.json version for patch"
      cd server/
      CURRENT_VERSION=$(node -p "require('./package.json').version") 
      echo "CURRENT_VERSION:  "$CURRENT_VERSION
      echo
      NEW_VERSION=$(npm version patch)
      echo "NEW_VERSION:  "$NEW_VERSION
      echo

      echo "Updating admin package.json version for patch"
      echo
      cd ../admin/
      CURRENT_VERSION=$(node -p "require('./package.json').version") 
      echo "CURRENT_VERSION:  "$CURRENT_VERSION
      echo
      NEW_VERSION=$(npm version patch)
      echo "NEW_VERSION:  "$NEW_VERSION
      echo

      echo "Updating client package.json version for patch" 
      echo 
      cd ../client/
      CURRENT_VERSION=$(node -p "require('./package.json').version") 
      echo "CURRENT_VERSION:  "$CURRENT_VERSION
      echo
      NEW_VERSION=$(npm version patch)
      echo "NEW_VERSION:  "$NEW_VERSION
      echo

      cd ../
      echo "....Pushing into Git...."
      echo
      # pushing changes into git with NEW_VERSION as tag
      git add */package*
      git commit -m "$NEW_VERSION"
      git push
      git tag "$NEW_VERSION"

   elif [ "$1" = "minor" ]; then
      #  Using npm version patch command to update the version 
      echo
      echo "Updating server package.json version for minor"
      cd server/
      CURRENT_VERSION=$(node -p "require('./package.json').version") 
      echo "CURRENT_VERSION:  "$CURRENT_VERSION
      echo
      NEW_VERSION=$(npm version minor)
      echo "NEW_VERSION:  "$NEW_VERSION
      echo

      echo "Updating admin package.json version for minor"
      echo
      cd ../admin/
      CURRENT_VERSION=$(node -p "require('./package.json').version") 
      echo "CURRENT_VERSION:  "$CURRENT_VERSION
      echo
      NEW_VERSION=$(npm version minor)
      echo "NEW_VERSION:  "$NEW_VERSION
      echo

      echo "Updating client package.json version for minor" 
      echo 
      cd ../client/
      CURRENT_VERSION=$(node -p "require('./package.json').version") 
      echo "CURRENT_VERSION:  "$CURRENT_VERSION
      echo
      NEW_VERSION=$(npm version minor)
      echo "NEW_VERSION:  "$NEW_VERSION
      echo

      cd ../
      echo "....Pushing into Git...."
      echo
      # pushing changes into git with NEW_VERSION as tag
      git add */package*
      git commit -m "$NEW_VERSION"
      git push
      git tag "$NEW_VERSION"

   else
      echo "Invalid argument"
      exit 1
   
   echo
   echo "....Script Executed Successfully...."
   fi
else
   echo
   echo $GIT_RESULT
   echo "...NEW CHANGES FOUND FROM REMOTE..."
   echo "...PLEASE REVIEW THE CHANGES AND TRY AGAIN...."
fi